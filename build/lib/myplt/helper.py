import os, sys
project_dir = os.path.abspath(os.path.join(os.path.dirname(__file__)))
sys.path.insert(0, os.path.join(project_dir, "../.."))

import ROOT
import numpy as np
import config
import utils.check
import utils.utils
import settings


previous_colors = []
already_adjusted = False
drawn_with_expression = 0
keep_alive = []

class textFrame(object):


  def __init__(self, xmin=None, ymin=None, xmax=None, ymax=None, align=None):

    # Initialize some members
    self.o_list = []
    dx, dy = settings.textframe.dx, settings.textframe.dy
    self.dt = settings.textframe.text_separation
    self.xmin, self.ymin, self.xmax, self.ymax = xmin, ymin, xmax, ymax

    # Get margins of current pad
    m_top, m_right, m_bottom, m_left = get_gpad_margins()

    if settings.textframe.x1 is not None and not self.xmin:
      self.xmin = settings.textframe.x1
    else:
      self.xmin = m_left + dx   
    if settings.textframe.x2 is not None and not self.xmax:
      self.xmax = settings.textframe.x2
    else:
      self.xmax =1.0 - m_right - dx
    if settings.textframe.y2 is not None and not self.ymax:
      self.ymax = settings.textframe.y2
    else:
      self.ymax = 1.0 - m_top - dy
    if settings.textframe.y1 is not None and not self.ymin:
      self.ymin = settings.textframe.y1
    else:
      self.ymin = m_bottom + dy

    # If a specific alignment is requested
    if align != None:
      if align.lower() == "left":
        self.xmin = m_left + dx
        self.xmax = 1.0 - m_right - dx
        self.ymax = 1.0 - m_top - dy
        self.ymin = m_bottom + dy
      if align.lower() == "right":
        self.xmin = 0.6
        self.xmax = 1.0 - m_right - dx
        self.ymax = 1.0 - m_top - dy
        self.ymin = m_bottom + dy


  def addHeader(self, text):

    return self.addText(text, text_size=0.06, text_color=1, text_font=72)


  def addExperiment(self, experiment="ATLAS", status="Internal"):

    return self.addText("#scale[%s]{#font[72]{%s} #font[42]{#scale[0.8]{#it{%s}}}}" % (settings.textframe.header_scale, experiment, status), text_size=0.06, text_color=1, text_font=72)


  def addText(self, text="", text_size=None, text_color=None, text_font=None):

    # Set some defaults
    text_size = settings.textframe.text_size
    text_color = settings.textframe.text_color
    text_font = settings.textframe.text_font
    # Get current number of entries in list
    n = len(self.o_list)
    # Get Tlatex object with text and coordinates
    if settings.textframe.align_y.lower() == "top2bottom":
      o = ROOT.TLatex(self.xmin, self.ymax - n*self.dt, text)
    elif settings.textframe.align_y.lower() == "bottom2top":
      o = ROOT.TLatex(self.xmin, self.ymin + n*self.dt, text)
    # Set some text properties
    o.SetTextSize(text_size)
    o.SetTextColor(text_color)
    o.SetTextFont(text_font)
    if settings.textframe.ndc:
      o.SetNDC()
    # Add to list of TLatex objects in this frame
    self.o_list.append(o)

    self.draw()
    return self


  def draw(self):

    for o in self.o_list: o.Draw()


  def reset (self, argument=None):

    if argument == None:
      for o in self.o_list:
        o.Delete()
      self.o_list = []
    elif isinstance(argument, str):
      self.o_list[argument].Delete()
      self.o_list.remove(argument)
    elif isinstance(argument, int):
      self.o_list[argument].Delete()
      del self.o_list[argument]

    return self


def add_time_stamp(xmin=settings.timestamp.xmin, ymin=settings.timestamp.ymin, text_size=settings.timestamp.text_size, text_color=settings.timestamp.text_color, text_font=settings.timestamp.text_font):

  from datetime import date
  today = date.today()
  # Textual month, day and year	
  date = today.strftime("%B %d, %Y")
  o = ROOT.TLatex()
  # Some properties
  o.SetTextSize(text_size)
  o.SetTextColor(text_color)
  o.SetTextFont(text_font)
  o.SetNDC()
  o.DrawLatex(xmin, ymin, date)
  return o


def add_axis(xmin, ymin, xmax, ymax, wmin, wmax, n_div, option, axis="y", title=None):

  # Which axis to use
  if axis.lower() == "x":
    axis_settings = settings.axis.x
  elif axis.lower() == "y":
    axis_settings = settings.axis.y
  else:
    axis_settings = settings.axis.z

  # Get axis
  axis = ROOT.TGaxis(xmin, ymin, xmax, ymax, wmin, wmax, n_div, option)
  if title:
    axis.SetTitle(title)
  # Set some properties
  axis.SetTitleSize(axis_settings.title_size)
  axis.SetTitleOffset(settings.axis.title_offset_h)
  axis.SetTitleFont(axis_settings.title_font)
  axis.SetTitleColor(axis_settings.title_color)
  axis.SetLabelSize(settings.axis.label_size)
  axis.SetLabelFont(axis_settings.label_font)
  axis.SetLabelOffset(settings.axis.label_offset)
  axis.CenterTitle(settings.axis.center_title)

  return axis


#def add_text(x, y, text, text_size=config.__text_size__["medium"], text_color=config.__text_color__, text_font=config.__text_font__, dt=config.__text_sep__, text_angle=0, ndc=True):
#
#  if not isinstance(text, list): text = [text]
#
#  for i_txt in range(len(text)):
#    t_l = ROOT.TLatex()
#    t_l.SetTextColor(text_color)
#    t_l.SetTextSize(text_size)
#    t_l.SetTextAngle(text_angle)
#    t_l.SetTextFont(text_font)
#    if ndc: t_l.SetNDC()
#    t_l.DrawLatex(x, y - i_txt*dt, text[i_txt])


def add_vertical_axis(wmin, wmax, x=None, n_div=510, title=None, option="+L"):

  # Get pad's margins
  ROOT.gPad.Update()
  l_m = ROOT.gPad.GetUxmin()
  r_m = ROOT.gPad.GetUxmax()
  t_m = ROOT.gPad.GetUymax()
  b_m = ROOT.gPad.GetUymin()
  # Disable ticks at this side
  ROOT.gPad.SetTicks(1,0)
  if x == None:
    x = r_m
  # Get axis
  axis = add_axis(x, b_m, x, t_m, wmin, wmax, n_div, option, "y", title)
  return axis


def add_horizontal_axis(wmin, wmax, y=None, n_div=510, title=None, option="-", xx1=None, xx2=None):

  # Absolute pad coordinates
  x1, x2 = ROOT.Double(0), ROOT.Double(0)
  y1, y2 = ROOT.Double(0), ROOT.Double(0)
  ROOT.gPad.GetRangeAxis(x1, y1, x2, y2)

  # Disable ticks at this side
  if y == None:
    y = y1
  # Get axis
  if not xx1 is None and xx2 is None:
    axis = add_axis(xx1, y, x2, y, wmin, wmax, n_div, option, "x", title)
  elif xx1 is None and not xx2 is None:
    axis = add_axis(x1, y, xx2, y, wmin, wmax, n_div, option, "x", title)
  elif not xx1 is None and not xx2 is None:
    axis = add_axis(xx1, y, xx2, y, wmin, wmax, n_div, option, "x", title)
  else:
    axis = add_axis(x1, y, x2, y, wmin, wmax, n_div, option, "x", title)
  return axis


#def add_vertical_line(x, ndc=True):
#
#  t_l = ROOT.TLine()
#  l_m = ROOT.gPad.GetLeftMargin()
#  r_m = 1.0 - ROOT.gPad.GetRightMargin()
#  t_m = 1.0 - ROOT.gPad.GetTopMargin()
#  b_m = ROOT.gPad.GetBottomMargin()
#  if not ndc:
#    x = (r_m-l_m)*((x-ROOT.gPad.GetUxmin())/(ROOT.gPad.GetUxmax()-ROOT.gPad.GetUxmin()))+l_m
#  t_l.DrawLineNDC(x, b_m, x, t_m)
#
#
#def add_horizontal_line(x, line_width=2, line_style=2, ndc=False):
#
#  ROOT.gPad.Update()
#  t_l = ROOT.TLine()
#  t_l.SetLineStyle(line_style)
#  t_l.SetLineWidth(line_width)
#  l_m = ROOT.gPad.GetLeftMargin()
#  r_m = 1.0 - ROOT.gPad.GetRightMargin()
#  t_m = 1.0 - ROOT.gPad.GetTopMargin()
#  b_m = ROOT.gPad.GetBottomMargin()
#  if not ndc:
#    x = (t_m-b_m)*((x-ROOT.gPad.GetUymin())/(ROOT.gPad.GetUymax()-ROOT.gPad.GetUymin()))+b_m
#  t_l.DrawLineNDC(l_m, x, r_m, x)


def add_grid(x=True, y=True):

  # Set grid with global properties
  ROOT.gPad.SetGrid(x,y)


def add_legend (xmin, ymin, xmax, ymax, entry=None, title=None, n_cols=1):

  # Define legend and set some properties
  legend = ROOT.TLegend(xmin, ymin, xmax, ymax)
  legend.SetBorderSize(settings.histo.legend.border_size)
  legend.SetTextFont(settings.histo.legend.font)
  legend.SetFillColorAlpha(settings.histo.legend.fill_color, settings.histo.legend.fill_color_alpha)
  legend.SetTextColor(settings.histo.legend.text_color)
  legend.SetTextSize(settings.histo.legend.text_size)
  legend.SetFillStyle(settings.histo.legend.fill_style)

  if title:
    if settings.histo.legend.entry_delimiter in title:
      title, align = title.split(settings.histo.legend.entry_delimiter)
      legend.SetHeader(title, align)
    else:
      legend.SetHeader("#splitline{%s}{}" % title, settings.histo.legend.header_align)

  # Check if histogram titles are supposed to be entries
  if settings.histo.legend.titles_are_entries and entry is not None:
    if all(not isinstance(o, (tuple, list)) for o in entry):
      if all(utils.check.inherits_from(o, ("TH1", "TF1", "TGraph", "TGraph2D", "THStack")) for o in entry):
        # Check if there is a tag
        for i, o in enumerate(entry):
          if settings.histo.legend.title_opt_delimiter not in o.GetTitle():
            entry[i] = (o, o.GetTitle())
          else:
            t = o.GetTitle().split(settings.histo.legend.title_opt_delimiter)
            entry[i] = (o, t[0], t[1])

###  # Check if histogram titles are supposed to be entries
###  if settings.histo.legend.titles_are_entries and entry is not None:
###    if all(not isinstance(o, (tuple, list)) for o in entry):
###      if all(utils.check.inherits_from(o, ("TH1", "TF1", "TGraph")) for o in entry):
###        # Check if there is a tag
###        for i, o in enumerate(entry):
###          if settings.histo.legend.title_opt_delimiter not in o.GetTitle():
###            entry[i] = (o, o.GetTitle())
###          else:
###            t = o.GetTitle().split(settings.histo.legend.title_opt_delimiter)
###            entry[i] = (o, t[0], t[1])


  # If entry is not given, return empty legend
  if entry is not None:
    if not isinstance(entry, (list, tuple)):
      entry = [entry]
    for e in entry:
      if any(component == None for component in e):
        continue
      # Get object
      obj = e[0]
      if e[0].InheritsFrom("TF1"):
        obj = obj.GetHistogram()
      # Check if entry also defines a draw option for legenda
      if settings.histo.legend.entry_delimiter in e[1] \
         and any(opt.lower() in e[1] for opt in settings.histo.legend.entry_options):
        comp = e[1].split(settings.histo.legend.entry_delimiter)
        # Remove empty lists
        comp = filter(None, comp)
        if len(comp) == 2:
          text, opt = comp
          if any(o.lower() in opt for o in settings.histo.legend.entry_options):
            legend.AddEntry(obj, text, opt)
      elif len(e) == 3:
        legend.AddEntry(obj, e[1], e[2])
      else:
        legend.AddEntry(obj, e[1], settings.histo.legend.entry_option)

  legend.SetNColumns(n_cols)

  return legend


def add_title(t_h, title="", axis="x"):

  if not axis.title() in ["X", "Y", "Z"]:
    print("[ERROR] Invalid axis `%s` [X/Y/Z]" % axis.title())
    sys.exit()
  # If title=None and axis=y, use standard title
  if title == "" and axis.title() == "Y" and not utils.check.inherits_from(t_h,("TF1", "TGraph", "TGraph2D", "TH2")):
    # Only keep decimal part of bin width
    x = "%e" % abs(int(t_h.GetBinWidth(1)) - t_h.GetBinWidth(1))
    pos = x.partition("-")[2]
    if pos:
      pos = int(pos)+1
    else:
      pos = 0
    # If x axis title is already defined, look for a unit
    x_title, unit = t_h.GetXaxis().GetTitle(), ""
    if x_title:
      if settings.title.unit_open in x_title and settings.title.unit_close in x_title:
        unit = x_title[x_title.find("[")+1:x_title.find("]")]
    # Ckeck if the histograms is normed to unity
    if not utils.check.is_normalized(t_h):
      title = "%s / %.0{pos}f".format(pos=pos) % (settings.histo.y_title_default, t_h.GetBinWidth(1))
    else:
      title = "%s / %.0{pos}f".format(pos=pos) % (settings.histo.y_title_default_normalized, t_h.GetBinWidth(1))
    if unit:
      title = "%s %s" % (title, unit)

  # Which axis to use
  if axis.lower() == "x": axis_settings = settings.axis.x
  elif axis.lower() == "y": axis_settings = settings.axis.y
  else: axis_settings = settings.axis.z

  # Set title
  t_axis = getattr(t_h, "Get%saxis" % axis.title())()
  t_axis.SetTitle(title)

  # Some title properties
  t_axis.SetTitleOffset(axis_settings.title_offset)


def make_me_pretty (histos):

  if not isinstance(histos, list): histos = [histos]
  
  for i in range(len(histos)):

    if histos[i].__class__.__name__ == "THStack": continue

    if "dummy" in histos[i].GetName().lower() or "ugly" in histos[i].GetName().lower() or "keep ugly" in histos[i].GetTitle().lower(): continue
    # Change the makrker?
    if settings.histo.change_marker:
      histos[i].SetMarkerStyle(settings.histo.marker_style + i)
    # Other properties
    # -- Line
    if histos[i].GetLineColor() == 602:
      histos[i].SetLineColor(settings.histo.line_color)
    if histos[i].GetLineStyle() == 1:
      histos[i].SetLineStyle(settings.histo.line_style)
    histos[i].SetLineWidth(settings.histo.line_width)
    # -- Marker
    histos[i].SetMarkerColor(settings.histo.marker_color)
    histos[i].SetMarkerSize(settings.histo.marker_size)
    histos[i].SetMarkerColor(settings.histo.marker_color)
    # -- Area
    histos[i].SetFillStyle(settings.histo.fill_style)
    histos[i].SetFillColorAlpha(settings.histo.fill_color, settings.histo.fill_color_alpha)

  for hist in histos:
    # -- Axis
    hist.GetXaxis().SetLabelSize(settings.axis.x.label_size)
    hist.GetYaxis().SetLabelSize(settings.axis.y.label_size)
    if "th2" in type(hist).__name__.lower():
      hist.GetZaxis().SetLabelSize(settings.axis.z.label_size)
    hist.GetXaxis().SetTitleSize(settings.axis.x.title_size)
    hist.GetXaxis().SetLabelFont(settings.axis.x.label_font)
    hist.GetYaxis().SetLabelFont(settings.axis.y.label_font)
    hist.GetYaxis().SetTitleSize(settings.axis.y.title_size)
    if "dummy" in hist.GetName().lower() or "ugly" in hist.GetName().lower() or "keep ugly" in hist.GetTitle().lower(): continue
    # -- Area
    hist.SetFillStyle(settings.histo.fill_style)
    hist.SetFillColorAlpha(settings.histo.fill_color, settings.histo.fill_color_alpha)
    print("2", settings.histo.fill_color_alpha)



def draw_histos (t_h_list, option=settings.histo.draw_option, draw_same=False, n_hist_drawn=0, uncert_band=None, redraw=None):

  option = option.replace("CUSTOM", "")

  if not isinstance(t_h_list, list):
    t_h_list = [t_h_list]

  # Sort list
#  t_h_list.sort(key=lambda x: x.GetMaximum(), reverse=True)


  if "!!!" in option:
    opt = option.replace("!!!", "")
    t_h_list[0].Draw(opt)
    return

  if "!" in option:
    option = option.replace("PLC", "").replace("PMC", "").replace("PFC", "")

  # Draw first histo in list that defines range
  for i_t_h in range(len(t_h_list)):

    if settings.histo.marker_list is not None:
      if len(settings.histo.marker_list) != 0:
        if i_t_h <= len(settings.histo.marker_list):
          t_h_list[i_t_h].SetMarkerStyle(settings.histo.marker_list[i_t_h])
        else:
          t_h_list[i_t_h].SetMarkerStyle(settings.histo.marker_list[-1])

    if settings.histo.line_color_list is not None:
      col = settings.histo.line_color_list[i_t_h]
      if "#" in str(settings.histo.line_color_list[i_t_h]):
        col = ROOT.TColor.GetColor(settings.histo.line_color_list[i_t_h])
      if len(settings.histo.line_color_list) != 0:
        if i_t_h <= len(settings.histo.line_color_list):
          t_h_list[i_t_h].SetLineColor(col)
          t_h_list[i_t_h].SetMarkerColor(col)
        else:
          t_h_list[i_t_h].SetLineColor(col)
          t_h_list[i_t_h].SetMarkerColor(col)
    elif ("PMC" in option or "PLC" in option or "PFC" in option or settings.histo.line_color_list is None) and "ugly" not in t_h_list[i_t_h].GetName().lower():
  #    if "!" in option:
      if n_hist_drawn > 1:
        if "!" not in option:
          global drawn_with_expression
          drawn_with_expression += 1
        ncol = int(ROOT.TColor.GetPalette().fN-1) / drawn_with_expression
      else:
        ncol = ROOT.TColor.GetPalette().fN-1
        ncol = 0

      if len(t_h_list) > 1:
        ncol = i_t_h * int(ROOT.TColor.GetPalette().fN-1) // (len(t_h_list)-1)

      if t_h_list[i_t_h].__class__.__name__ != "THStack":
        t_h_list[i_t_h].SetLineColor(ROOT.TColor.GetPalette()[ncol])
        t_h_list[i_t_h].SetMarkerColor(ROOT.TColor.GetPalette()[ncol])
        t_h_list[i_t_h].SetFillColorAlpha(ROOT.TColor.GetPalette()[ncol], settings.histo.fill_color_alpha)

    if i_t_h == 0 and not draw_same:

      if uncert_band is not None:
        for h in uncert_band:
          h_1 = h.Clone("xxxxxx")
          h_1.Divide(h_1)
          h.Add(h_1)
#          for k in range(h.GetNbinsX()):
#            if h.GetBinContent(k+1) <= 0:
#              h.SetBinContent(k+1, 1e-15)
#        uncert_band.sort(key=lambda x: x.Integral(), reverse=True)
        th_up = t_h_list[i_t_h].Clone("err_up")
        th_up.Multiply(uncert_band[1])
        th_dn = t_h_list[i_t_h].Clone("err_dn")
        th_dn.Multiply(uncert_band[0])
        uncert_band = [th_up, th_dn]
        keep_alive.append(uncert_band)
        t_h_list[i_t_h].Draw(option)
        # Draw stuff


        uncert_band[0].SetFillColorAlpha(settings.error.fill_color, settings.error.fill_color_alpha)
        uncert_band[0].SetFillStyle(settings.error.fill_style)
        uncert_band[0].SetLineWidth(1)
        uncert_band[0].SetLineColor(settings.error.line_color)
        uncert_band[0].Draw("HIST SAME")
        uncert_band[1].SetFillColorAlpha(0, 1)
        uncert_band[1].SetFillStyle(1001)
        uncert_band[1].SetLineWidth(0)
        uncert_band[1].Draw("HIST SAME")
        t_h_list[i_t_h].Draw("AXIS SAME")
        uncert_band[0].SetDirectory(0)
        uncert_band[1].SetDirectory(0)

      if not ( " P " in option and "HIST" in option):
        t_h_list[i_t_h].Draw(option.replace("!", "") + (" SAME" if uncert_band is not None else ""))
      else:
        t_h_list[i_t_h].Draw(option.replace(" P ", "") + (" SAME" if uncert_band is not None else ""))
        t_h_list[i_t_h].Draw("%s SAME" % option.replace("A", "").replace("PLC", "").replace("PMC", "").replace("PFC", ""))
        if settings.histo.line_color_list is None and "ugly" not in t_h_list[i_t_h].GetName().lower():
          t_h_list[i_t_h].SetLineColor(ROOT.gStyle.GetColorPalette(i_t_h))
          t_h_list[i_t_h].SetMarkerColor(ROOT.gStyle.GetColorPalette(i_t_h))
    else:
      if not ( " P " in option and "HIST" in option):
        t_h_list[i_t_h].Draw("%s SAME" % option.replace("!", "").replace("A", ""))
      else:
        t_h_list[i_t_h].Draw("%s SAME" % option.replace(" P ", ""))
        t_h_list[i_t_h].Draw("%s SAME" % option.replace("A", "").replace("PLC", "").replace("PMC", "").replace("PFC", "").replace(" P ", "").replace("A", ""))
        if settings.histo.line_color_list is None and "ugly" not in t_h_list[i_t_h].GetName().lower():
          t_h_list[i_t_h].SetLineColor(ROOT.gStyle.GetColorPalette(i_t_h))
          t_h_list[i_t_h].SetMarkerColor(ROOT.gStyle.GetColorPalette(i_t_h))
        t_h_list[i_t_h].Draw("%s SAME" % option.replace("A", ""))

  if redraw is not None:
    t_h_list[redraw].Draw("%s SAME" % option.replace("A", "").replace("PLC", "").replace("PMC", "").replace("PFC", ""))

#def fill_area(t_h, color=config.__fill_color__, fill_style=config.__fill_style__, alpha=config.__fill_color_alpha__):
#
#  t_h.SetFillColorAlpha(color, alpha)
#  t_h.SetFillStyle(fill_style)
#

#def get_std_header(experiment=config.__experiment__, status=config.__status__):
#
#  tf = textFrame(**config.__text_std_pos)
#  tf.addText("#font[72]{%s} #font[42]{#scale[0.8]{#it{%s}}}" % (experiment, status), text_size=0.06, text_color=1, text_font=72)
#  return tf
#
#
def get_gpad_margins():

  if ROOT.gPad:
    ROOT.gPad.Update()
    return ROOT.gPad.GetTopMargin(), ROOT.gPad.GetRightMargin(), ROOT.gPad.GetBottomMargin(), ROOT.gPad.GetLeftMargin()
  else:
    return settings.pad.top_margin, settings.pad.right_margin, settings.pad.bottom_margin, settings.pad.left_margin

#
#def get_gpad_range():
#
#  ROOT.gPad.Update()
#  return getattr(ROOT.gPad, "GetUxmin")(), getattr(ROOT.gPad, "GetUymin")(), getattr(ROOT.gPad, "GetUxmax")(), getattr(ROOT.gPad, "GetUymax")()
#
#
#def get_canv_pad(cname="c", pname="p", ww=700, wh=500, keep_ratio=True, overlay=config.__use_overlaying__):
#
#  prefix = ""
#  if config.__use_overlaying__:
#    prefix = "overlay_no%d" % config.__overlay_counter__
#    config.inc_overlay_counter()
#  # Create canvas with one pad on top
#  t_c = ROOT.TCanvas(prefix+cname, "", ww, wh)
#  t_p = ROOT.TPad(prefix+pname, "", 0.0, 0.0, 1.0, 1.0)
#  t_p.SetFillColor(0)
#  t_p.SetFillStyle(4000)
#  t_p.SetFrameFillStyle(0)
#  t_p.Draw()
#  t_p.cd()
#
#  # Adjust magin(s) based on global configuration
#  w_dict = {side : 1 for side in config.__margin_pad__}
#  for side in config.__margin_pad__:
#    margin = config.__margin_pad__[side]
#    if (overlay or config.__use_overlaying__) and side == "Right":
#      margin = config.__margin_pad__["Left"]
#    getattr(t_p, "Set%sMargin" % side)(w_dict[side] * margin)
#
#  return t_c, t_p


def y_axis_min (o_list):

  if not isinstance(o_list, list):
    o_list = [o_list]

  # Get maximum
  max_list = [9999999]
  for o in o_list:
    if o.InheritsFrom("TH1"):
      max_list.append(o.GetBinContent(o.GetMinimumBin()))
    elif o.InheritsFrom("TGraph") or o.InheritsFrom("TGraphAsymmErrors"):
      max_list.append(ROOT.TMath.MinElement(o.GetN(), o.GetY()))
  return min(max_list)


def adjust_y (o_list, ymin=None, ymax=None, is_ratio=False, ratio=2.0):

#  print(">>>>", o_list, ymin, ymax)
#
#  global already_adjusted
#
#  if already_adjusted:
#    return
#  else:
#    already_adjusted = True
  

  if not isinstance(o_list, list):
    o_list = [o_list]

  if o_list[0].__class__.__name__ == "THStack": return

  # If min and/or max have not been specified, compute limits
  ROOT.gPad.Update()
  y_min, y_max = y_axis_min(o_list), y_axis_max(o_list)
  if ymin != None:
    y_min = ymin
  if ymax != None:
    y_max = ymax


  # Make a quick check if Log is appropriate
  if ROOT.gPad.GetLogy():
   if y_min == 0:
     y_min = 1
   if y_min < 0:
     ROOT.gPad.SetLogy(0)
   elif np.log10(y_max/y_min) < settings.auto.OoMthreshold:
     ROOT.gPad.SetLogy(0)

  # Get first object
  o = o_list[0]

  # Get new y limits of pad
  if ROOT.gPad.GetLogy():
    if ymax is None:
      if y_min < 1: 
        y_min=1
      n = np.log10(y_max/y_min)
      y_max = 10**(1.5*n)
    if o.InheritsFrom("TH1") or o.InheritsFrom("TF1"):
      o.SetMinimum(y_min)
      o.SetMaximum(y_max)
    if o.InheritsFrom("TGraph") or o.InheritsFrom("TGraphAsymmErrors"):
      o.GetHistogram().SetMinimum(y_min)
      o.GetHistogram().SetMaximum(y_max)
  else:
    if ymin is None:
      y_min = y_min / ratio
      if y_min < 0:
        y_min = y_min * float(ratio) - y_max
    if is_ratio:
      y_min = 0.4
    if ymax == None:
      if y_max > 1:
        y_max = y_max * float(ratio) - y_min
      else:
        y_max = y_max * float(ratio)
    if o.InheritsFrom("TH1") or o.InheritsFrom("TF1") or o.InheritsFrom("TH2"):
      o.SetMinimum(y_min)
      o.SetMaximum(y_max)
    if o.InheritsFrom("TGraph") or o.InheritsFrom("TGraphAsymmErrors"):
      o.GetHistogram().SetMinimum(y_min)
      o.GetHistogram().SetMaximum(y_max)

  return (y_min, y_max)


def y_axis_max(o_list):

  if not isinstance(o_list, list):
    o_list = [o_list]

  # Get maximum
  max_list = [-9999999]
  for o in o_list:
    if o.InheritsFrom("TH1"):
      max_list.append(o.GetBinContent(o.GetMaximumBin()))
    elif o.InheritsFrom("TGraph") or o.InheritsFrom("TGraphAsymmErrors"):
      max_list.append(ROOT.TMath.MaxElement(o.GetN(), o.GetY()))
  return max(max_list)


def save_canvas(t_c, fname=settings.file.output, tex=settings.file.save_tex):


  settings.set_same_canvas(False)

  # Make sure that the respective directory exists
  path = os.path.dirname(fname)
  utils.utils.mkdir(path)

  # Remove some artefacts from name
  fname = fname.replace("clone_", "")

  ROOT.__class__.gErrorIgnoreLevel = 5999

  # Add extension to file name
  if not isinstance(settings.file.extension, list):

    valid_extension = False
    for extension in settings.file.extensions:
      if fname.endswith("."+extension):
        valid_extension = True

    if not fname.endswith("."+settings.file.extension) and not valid_extension:
      fname += "."+settings.file.extension

    # Save canvas
    if settings._book_mode.lower() == "start":
      t_c.SaveAs(fname + "[") 
      
    elif settings._book_mode.lower() == "close":
      t_c.SaveAs(fname + "]") 
    else:
      t_c.SaveAs(fname) 

  else:
    for extension in settings.file.extension:
      if not fname.endswith("."+extension):
        fname += "."+extension
      # Save canvas
      if settings._book_mode.lower() == "start":
        t_c.SaveAs(fname + "[") 
        
      elif settings._book_mode.lower() == "close":
        t_c.SaveAs(fname + "]") 
      else:
        t_c.SaveAs(fname) 
      fname = fname.replace(".%s" % extension, "")
           

  # If requested, save as tex file as well
  if tex:
    fname_tex = fname.replace(settings.file.extension, "_pgfplots.tex")
    t_c.SaveAs(fname_tex)


class textHeader(ROOT.TPaveText):

  def __init__(self, text=None, text_size=settings.textheader.text_size, text_color=settings.textheader.text_color, text_font=settings.textheader.text_font):

    # List so store all lines to be drawn
    self.rows = []

    # Get margins of current pad
    m_top, m_right, m_bottom, m_left = get_gpad_margins()

    if settings.textheader.y[1] is not None:
      y2 = settings.textheader.y[1]
    elif settings.textheader.y2 is not None:
      y2 = settings.textheader.y2
    else:
      y2 = 1
    if settings.textheader.x[1] is not None:
      x2 = settings.textheader.x[1]
    elif settings.textheader.x2 is not None:
      x2 = settings.textheader.x2
    else:
      x2 = 1-m_right
    if settings.textheader.y[0] is not None:
      y1 = settings.textheader.y[0]
    elif settings.textheader.y1 is not None:
      y1 = settings.textheader.y1
    else:
      y1 = 1-m_top
    if settings.textheader.x[0] is not None:
      x1 = settings.textheader.x[0]
    elif settings.textheader.x1 is not None:
      x1 = settings.textheader.x1
    else:
      x1 = m_left

    # Initialize a TPaveText object
    ROOT.TPaveText.__init__(self, x1, y1, x2, y2, settings.textheader.option)
    
    self.SetTextFont(text_font)
    self.SetFillStyle(settings.textheader.fill_style)
    self.SetBorderSize(settings.textheader.border_size)
    self.SetMargin(settings.textheader.margin)
    if text:
      # Split text based on `new line delimiters` (cf. definition in settings)
      for line in text.split(settings.textheader.delimiter_new_line):
        self.AddText(line)
        self.rows(line)
      self.Draw()


  def addRow(self, text):
    self.SetTextSize(settings.textheader.text_size)
    self.AddText(text)
    self.Draw()
    return self

 
  def setTextAlign(self, align="left"):

    if align.lower() == "left":
      self.SetTextAlign(11)
    else:
      self.SetTextAlign(align)


  def addExperiment(self, experiment="ATLAS", status="Internal", text=""):

    self.AddText("#scale[%s]{#font[72]{%s} #font[42]{#scale[0.8]{#it{%s}}}}  %s" % (settings.textframe.header_scale, experiment, status, text))
    self.Draw()
    return self


  def draw(self):

    self.Draw()
    return self

  
  def reset (self, argument=None):

    if argument == None:
      self.rows = []
      self.Clear()
    elif isinstance(argument, str):
      self.rows.remove(argument)
      for row in rows: self.addRow(row)
    elif isinstance(argument, int):
      del self.rows[argument]
      for row in rows: self.addRow(row)



def use_log(t_h):

  if not isinstance(t_h, list): t_h = [t_h]

  # Do some checks
  if not all(h.InheritsFrom("TH1") for h in t_h):
    return False

  # TODO: Fix this
  if "TH2" in str(type(t_h[0])):
    return False

  # Define list of orders of magnitude at which area hits threshold
  orders = []
  # Loop over histograms
  for h in t_h:
    # Get the total area
    integral_tot = h.Integral()
    integral = 0
    for i in range(1, h.GetNbinsX()):
      integral += h.Integral(1, i)
    if integral/float(integral_tot) > settings.auto.log_th:
      orders.append(np.log10(h.GetBinContent(i)/h.GetMaximum()))
  if len(orders) == 0 or all(np.abs(val) < settings.auto.logoom for val in orders):
    return False
  else:
    return True


#if __name__ == "__main__":
#
#   # initialize histogram
#   t_h = ROOT.TH1D("gauss", "", 50, -5, 5)
#
#   # Get data
#   x = np.random.normal(0, 1, 100000)
#
#   # Fill into histogram
#   for i in x: t_h.Fill(i)
#
#   # Save
#   t_c, t_p = get_canv_pad()
#   
#   # Adjust height
#   integral = t_h.Integral()
#   t_h.Scale(1.0/integral)
#   adjust_y(t_h)
#   draw_histos(t_h)
#   v_axis = add_vertical_axis(0, 1)
#   h_axis = add_horizontal_axis(0, 1)
#   v_axis.Draw()
#   h_axis.Draw()
#   atlas_tf = get_std_header()
#   atlas_tf.draw()
#   add_title(t_h, "Some axis of ordinates [Unit :)]", axis="x")
#   add_title(t_h, axis="y")
#   fill_area(t_h)
#   leg = add_legend(entry=[(t_h, "TH1D entry", "l")])
#   leg.Draw()
#   # Add a text box
#   txt = textFrame(ymax=0.7)
#   txt.addText("Text")
#   txt.addText("Text")
#   txt.addText("Text")
#   txt.addText("Text")
#   txt.addText("Text")
#   txt.addText("Text")
#   add_grid()
#   txt.draw()
#   # Some text on top
#   tot = text_on_top("This is some text that may easily extend over several lines...;like here...;or here...")
#   tot.Draw()
#   # Add a time state (date of creation) to plot
#   add_time_stamp()
#   t_c.SaveAs("adjuested_height_lin.pdf")
#
#   # Adjust height
#   t_c.SetLogy()
#   adjust_y(t_h)
#   t_h.Draw()
#   txt.draw()
#   # Some text on top
#   tot = text_on_top("Text1;Text2;Text3")
#   # Add a time state (date of creation) to plot
#   add_time_stamp()
#   # Add a grid
#   add_grid()
#   tot.Draw()
#   t_c.SaveAs("adjuested_height_log.pdf")


def is_square(apositiveint):

  x = apositiveint // 2
  seen = set([x])
  while x * x != apositiveint:
    x = (x + (apositiveint // x)) // 2
    if x in seen: return False
    seen.add(x)
  return True
