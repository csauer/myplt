import os, sys
import ntpath
import collections
import itertools

# Scientific import
import ROOT
import numpy as np
import math

# Custom imports
import utils.utils
import utils.check
import helper
import settings
import myroot.fio


# Path to this script
script_path = os.path.dirname(__file__)
ROOT.gStyle.SetOptStat(0)


KEEP_PADS = []


def is_version18 ():

  version = ROOT.gROOT.GetVersion().split("/")
  version = int(version[0].split(".")[1])
  if version > 17:
    return True
  else:
    return False
  


def _fname_or_file(f, tfiles):

  # Check if it is a ROOT file or a path to root file
  if not isinstance(f, ROOT.TFile):
    # Check if file is already in container
    files = [t_f for t_f in tfiles if f==t_f.GetName()]
    if len(files) == 1:
      f = files[0]
    else:
      f = ROOT.TFile(f, "READ")
  else:
    # Check if file is already in container
    files = [t_f for t_f in tfiles if f.GetName()==t_f.GetName()]
    if len(files) == 1:
      f = files[0]

  return f


class Canvas (ROOT.TCanvas):

  # Counter
  _ids = itertools.count(0)

  def __init__(self, canv=None, pad=None, canv_name="", n_pads=1):

    # Check defualt
    canv = (settings.canvas.defw, settings.canvas.defh) if canv == None else canv
    pad = (settings.pad.x1, settings.pad.y1, settings.pad.x2, settings.pad.y2) if pad == None else pad

    super(Canvas, self).__init__("canvas_%s" % Canvas._ids, "", canv[0], canv[1])
    # Set some properties of the canvas
    self.id = next(self._ids)
    if canv_name != "":
      self.SetName(canv_name)
    else:
      self.SetName("canvas_%s" % self.id)

    # Check if the canvas is supposed to be splitted
    if n_pads > 1: self.Divide(int(np.sqrt(n_pads)), int(np.sqrt(n_pads)))
      
    # Create default pad
    self.cd()
    self.pads = collections.OrderedDict([(ROOT.TPad(settings.pad.default_name, "", pad[0], pad[1], pad[2], pad[3]), [])])
    for pad in self.pads:
      pad.Draw()
      pad.cd()
    # Set Pad properties
    if settings.pad.grid:
      settings.pad.grid_x, settings.pad.grid_y = settings.pad.grid
    ROOT.gPad.SetGridx(settings.pad.grid_x)
    ROOT.gPad.SetGridy(settings.pad.grid_y)
    # TODO: Make this general
    ROOT.gPad.Update()
    ROOT.gStyle.SetFrameFillStyle(0)
    self.SetFillStyle(4000)
    self.SetFillColor(0)
    ROOT.gPad.SetFillColor(0)
    

  def add2pad(self, obj, pad_name=None, overwrite=True):

    if not isinstance(obj, list): 
      obj = [obj]
    if not pad_name:
      pad_name = ROOT.gPad.GetName()

    for o in obj:
      self.pads[self.getPad(pad_name)].append(o)


  def addPad(self, name, xmin, ymin, xmax, ymax, title="", pad_name=None):

    # If no pad name is given, draw on canvas
    if not pad_name or pad_name not in self.getPadNames():
      self.cd()
    else:
      self.getPad(pad_name).cd()

    # Check if this pad already exists
    if name in self.getPadNames():
      print("[WARNING] A pad with the name `%s` already exists" % name)
      return 

    # Add pad
    self.pads[ROOT.TPad(name, title, xmin, ymin, xmax, ymax)] = []

    # Apply some properties

    # Apply some properies
    self.getPad(name).SetFillStyle(settings.pad.fill_style)
    self.getPad(name).SetFrameFillStyle(settings.pad.frame_fill_style)

    # Make this the current pad
    self.getPad(name).Draw()
    self.getPad(name).cd()
    ROOT.gPad.SetFillStyle(4000)
    ROOT.gPad.SetFillColor(0)


  def cdPad(self, pad_name=settings.pad.default_name):
 
    self.getPad(self, pad_name).cd()


  def getPadNames(self):

    return [pad.GetName() for pad in self.pads]


  def getPad(self, name=None):

    if name in self.getPadNames():
      return filter(lambda x : x.GetName()==name, self.pads)[0]
    else:
      return filter(lambda x : x.GetName()==ROOT.gPad.GetName(), self.pads)[0]


class Figure (Canvas, myroot.fio.FileIo):


  color_palette_inv = False

  def __init__ (self, outfile="output.pdf", canv=None, pad=None, n_pads=1):

    Canvas.__init__(self, canv=canv, pad=pad, n_pads=n_pads)
    myroot.fio.FileIo.__init__(self)
    # Update path and filename
    self.path, self.fname = ntpath.split(outfile)
    # Create the directory
    if self.path: utils.utils.mkdir(self.path)
    # Container to keep root files open
    self.tfiles = []
    # Container with additional drawable objects
    self.drawables = []
    self.already_drawn = []
    # has common text been added?
    self.common_text_added = False
    self.common_head_added = False
    # Range has been set 
    self.custom_range_y = None
    # Globla draw option
    self.draw_option=None
    self.color_palette=None
    self.color_palette_inv = False
    # Counters
    self.n_haxis = 0


  def addHaxis(self, wmin, wmax, y=None, n_div=510, title=None, option="-", update_margin=True, dy=1.6, x1=None, x2=None, same=False, no_labels=False):

    if update_margin and not same:
      ROOT.gPad.SetBottomMargin(ROOT.gPad.GetBottomMargin()+0.03)

    # New pad for this axis
    pad_main = ROOT.gPad
    pad = ROOT.gPad.Clone("axis")
    pad.Draw()
    pad.cd()
    pad.SetFrameFillStyle(0)
    pad.SetBorderSize(0)
    pad.SetFrameLineWidth(0);
    pad.SetFrameBorderMode(0)

    # Increase axis counter
    if same is False:
      self.n_haxis += 1

    if y is None:
      # Absolute pad coordinates
      tmpx1, tmpx2 = ROOT.Double(0), ROOT.Double(0)
      y1, y2 = ROOT.Double(0), ROOT.Double(0)
      pad.GetRangeAxis(tmpx1, y1, tmpx2, y2)
      y = y1 - (y2-y1) * (self.GetWindowHeight()-pad.GetWh())/float(self.GetWindowHeight()) * self.n_haxis * dy

    # Get axis
    axis = helper.add_horizontal_axis(wmin, wmax, y, n_div, title, option, x1, x2)
    if no_labels:
      axis.SetLabelOffset(-999)
      axis.SetTitleOffset(0.4*axis.GetTitleOffset())
    axis.Draw()
    self.add2pad(axis)
    ROOT.gPad = pad_main
    ROOT.gPad.Update()

    return self


  def addVaxis(self, wmin, wmax, x=None, n_div=510, title=None, option="+L", update_margin=True):

    self.Update()
    axis = helper.add_vertical_axis(wmin, wmax, x, n_div, title, option)
    axis.Draw()

    self.add2pad(axis)    
    return self 


  def addAxisTitle(self, hist=None, title_x=None, title_y=None, title_z=None):
 
    if hist == None:
      hist = filter(lambda x : utils.check.inherits_from(x, settings.check_obj), self.pads[self.getPad()])

    if not isinstance(hist, list): hist = [hist]

    # Add title
    if title_y != None and title_x == None:
      for h in hist:
        helper.add_title(h, title_y, "y")
    elif title_y == None and title_x != None:
      for h in hist:
        helper.add_title(h, title_x, "x")
        helper.add_title(h, axis="y")
    elif title_y is not None and title_x is not None:
      for h in hist:
        helper.add_title(h, title_x, "x")
        helper.add_title(h, title_y, "y")

    if title_z is not None:
      if hasattr(h, "GetZaxis") and utils.check.inherits_from(h, "TH2"):
        helper.add_title(h, title_z, "z")
      else:
        t_l = ROOT.TLatex(0.97, 0.5, title_z)
        t_l.SetNDC()
        t_l.SetTextSize(h.GetXaxis().GetTitleSize())
        t_l.SetTextFont(h.GetXaxis().GetTitleFont())
        t_l.SetTextAngle(90)
        t_l.Draw()
        self.add2pad(t_l)

    return self


  def addPlotOnPlot (self, hist, option, x1=0, y1=0, x2=1, y2=1, log=(False, False, False), margin=0.16, add2pad=None):
  
    # Get the pad to which to add the ratio
    pad = self.getPad(add2pad)
    self.addPad("%s_plt2plt" % pad.GetName(), x1, y1, x2, y2, pad_name=add2pad)
  
    pad_ratio = self.getPad("%s_plt2plt" % pad.GetName())
    pad_ratio.Draw()
    pad_ratio.cd()
    pad_ratio.SetRightMargin(margin)
    pad_ratio.SetFrameFillStyle(1001)
    hist.Draw(option)
    pad_ratio.SetLogx(log[0])
    pad_ratio.SetLogy(log[1])
    pad_ratio.SetLogz(log[2])
    pad_ratio.Update()
    self.add2pad(hist.Clone("%s_clone" % hist.GetName()))
    return self


  def addColorPalette(self, min, max, ndiv=None, margin=0.16, add2pad=None, y1=0, y2=1):

    if ndiv is None: ndiv = int(max-min)

    # Get the pad to which to add the ratio
    pad = self.getPad(add2pad)
    pad.SetRightMargin(margin)
    # Add new pad
    self.addPad("%s_palette" % pad.GetName(), 0, y1, 1, y2, pad_name=add2pad)
    pad_ratio = self.getPad("%s_palette" % pad.GetName())
    pad_ratio.Draw()
    pad_ratio.cd()
    pad_ratio.SetRightMargin(margin)

    tmp_h2 = ROOT.TH2F("ONLY_FOR_PALETTE", "", 2, 0, 1, 2, 0, 1)
    tmp_h2.SetBinContent(0, 0, min)
    tmp_h2.SetBinContent(3, 3, max)
    tmp_h2.SetMaximum(max)
    tmp_h2.SetMinimum(min)
    tmp_h2.SetContour(ndiv)

    pad_ratio.SetTicks(0,0)
    pad_ratio.SetFrameBorderMode(0)
    pad_ratio.SetFrameFillColor(0)
    pad_ratio.SetFrameFillStyle(0)
    pad_ratio.SetFrameBorderSize(0)
    pad_ratio.SetFrameLineWidth(0)
    pad_ratio.SetFrameLineColor(0)

    tmp_h2.GetXaxis().SetLabelOffset(-999)
    tmp_h2.GetXaxis().SetTickLength(0)
    tmp_h2.GetYaxis().SetLabelOffset(-999)
    tmp_h2.GetYaxis().SetTickLength(0)

    tmp_h2.Draw("COLZ SAME")

    tmp_h2.GetZaxis().SetTitleSize(settings.axis.z.title_size)
    tmp_h2.GetZaxis().SetTitleOffset(settings.axis.z.title_offset_h)
    tmp_h2.GetZaxis().SetTitleFont(settings.axis.z.title_font)
    tmp_h2.GetZaxis().SetTitleColor(settings.axis.z.title_color)
    tmp_h2.GetZaxis().SetLabelSize(settings.axis.z.label_size)
    tmp_h2.GetZaxis().SetLabelFont(settings.axis.z.label_font)
    tmp_h2.GetZaxis().SetLabelOffset(settings.axis.z.label_offset)
    tmp_h2.GetZaxis().CenterTitle(settings.axis.z.center_title)
    tmp_h2.GetXaxis().SetAxisColor(0, 0)
    tmp_h2.GetYaxis().SetAxisColor(0, 0)

    pad_ratio.Update()
    self.add2pad(tmp_h2)
    return self


  def addDrawable(self, obj):

    if not isinstance(obj, list): obj = [obj]
    for o in obj:
      o.Draw()
      self.drawables.append(o)
    return self


  def addMarker(self, x, y, num, text="", color=settings.histo.marker_color, size=settings.histo.marker_size):

    m = ROOT.TMarker(x, y, num)
    m.SetNDC()
    m.Draw()
    m.SetMarkerSize(size)
    m.SetMarkerColor(color)
    self.drawables.append(m)
    if text != "":
      t_l = ROOT.TLatex(x+0.02, y-0.009, text)
      t_l.SetNDC()
      t_l.Draw()
      self.drawables.append(t_l)
    return self


  def histMatrix (self, histos, title_x=None, title_y=None, title_z=None, subcaption_x=[], subcaption_y=[], log=(0, 0), n=(None, None)):

    if histos is None:
      histos = filter(lambda x : utils.check.inherits_from(x, settings.check_obj), self.pads[self.getPad()])

    if settings._book_mode.lower() == "start":
      KEEP_PADS.append(self.pads[self.getPad(None)])
      self.pads[self.getPad(None)] = []

    nx, ny = None, None
    if len(histos) == 1:
      self.addHist(histos)
      return self
    elif len(histos) == 2:
      nx, ny = 2, 1
    elif len(histos) == 3:
      nx, ny = 3, 1
    else:
      nx, ny = len(histos)//int(math.sqrt(len(histos))), len(histos)//int(math.sqrt(len(histos)))
      if nx*ny != len(histos) and (len(histos) % 2 != 0):
        nx -= 1
      if len(histos) % int(math.sqrt(len(histos))) != 0:
        ny += 1

    if n[0]: nx = n[0]
    if n[1]: ny = n[1]

    # Range for pads
    pad_range_x, pad_range_y = np.linspace(0.10, 0.83, nx+1), np.linspace(0.89, 0.15, ny+1)

    for i in range(nx):
      for j in range(ny):
        k = i*(ny) + j
        if k >= len(histos): break
        # Each histogram gets it's own pad
        self.addPad("matrix_%s" % k, pad_range_x[i], pad_range_y[j], pad_range_x[i+1], pad_range_y[j+1])
        # Add this histo to this pad
        self.add2pad(histos[k])
        self.getPad("matrix_%s" % k).SetRightMargin(0)
        self.getPad("matrix_%s" % k).SetLeftMargin(0)
        self.getPad("matrix_%s" % k).SetBottomMargin(0)
        self.getPad("matrix_%s" % k).SetTopMargin(0)
        if log[1] != 0:
          self.getPad("matrix_%s" % k).SetLogy(1)
        helper.draw_histos(histos[k], self.draw_option)
#        print(i, j, histos[k].GetName(), histos[k].GetBinContent(1,1))
        if hasattr(histos[k], "GetZaxis"):
          histos[k].GetZaxis().SetLabelOffset(999)
          histos[k].GetZaxis().SetLabelSize(0)
          histos[k].GetZaxis().SetTickLength(0)
#          print(histos[k], pad_range_x[i], pad_range_y[j], pad_range_x[i+1], pad_range_y[j+1])

#        scale = self.getPad("pad0").GetWNDC()*self.getPad("pad0").GetHNDC()
#        histos[k].GetXaxis().SetLabelSize(histos[k].GetXaxis().GetLabelSize()/scale)  
#        histos[k].GetYaxis().SetLabelSize(histos[k].GetYaxis().GetLabelSize()/scale)  
#        histos[k].GetXaxis().SetTitleSize(histos[k].GetXaxis().GetTitleSize()/scale)  
#        histos[k].GetYaxis().SetTitleSize(histos[k].GetYaxis().GetTitleSize()/scale)  
#        histos[k].GetYaxis().SetTitleOffset(histos[k].GetYaxis().GetTitleOffset()*scale)

    self.getPad("pad0").cd()
    if title_x is not None:
      t_x = ROOT.TPaveText(0.2, 0.03, 0.80, 0.07, "NDC")
      txt = t_x.AddText(title_x)
      t_x.Draw()
      t_x.SetFillStyle(3000)
      t_x.SetBorderSize(0)
      txt.SetTextSize(settings.axis.x.title_size)
      txt.SetTextFont(settings.axis.x.title_font)
      self.add2pad(t_x)
    if title_y is not None:
      t_y = ROOT.TPaveText(0.01, 0.15, 0.04, 0.80, "NDC")
      txt = t_y.AddText(title_y)
      t_y.Draw()
      t_y.SetFillStyle(3000)
      t_y.SetBorderSize(0)
      txt.SetTextAngle(90)
      txt.SetTextSize(settings.axis.y.title_size)
      txt.SetTextFont(settings.axis.y.title_font)
      self.add2pad(t_y)
    if title_z is not None:
      t_z = ROOT.TPaveText(0.94, 0.15, 0.99, 0.80, "NDC")
      txt = t_z.AddText(title_z)
      t_z.Draw()
      t_z.SetFillStyle(3000)
      t_z.SetBorderSize(0)
      txt.SetTextAngle(90)
      txt.SetTextSize(settings.axis.z.title_size)
      txt.SetTextFont(settings.axis.z.title_font)
      self.add2pad(t_z)

    for i in range(len(subcaption_x)):
      if i == nx: break
      txx = ROOT.TPaveText(pad_range_x[i], 0.12 , pad_range_x[i+1], 0.1, "NDC")
      txt = txx.AddText(subcaption_x[i])
      txx.Draw()
      txx.SetFillStyle(3000)
      txx.SetBorderSize(0)
      txt.SetTextSize(settings.axis.x.title_size)
      txt.SetTextFont(settings.axis.x.title_font)
      self.add2pad(txx)
    for i in range(len(subcaption_y)):
      if i == ny: break
      txx = ROOT.TPaveText(0.07, pad_range_y[i], 0.07, pad_range_y[i+1], "NDC")
      txt = txx.AddText(subcaption_y[i])
      txx.Draw()
      txx.SetFillStyle(3000)
      txx.SetBorderSize(0)
      txt.SetTextSize(settings.axis.y.title_size)
      txt.SetTextFont(settings.axis.y.title_font)
      txt.SetTextAngle(90)
      self.add2pad(txx)

    # Check if common text has been set
    #if settings._common_text != None and not self.common_text_added:
    if settings._common_text != None:
      settings._common_text.draw()
      self.common_text_added = True
    #if settings._common_head != None and not self.common_head_added: 
    if settings._common_head != None: 
      settings._common_head.draw()
      self.common_head_added = True

    return self


  def addHist (self, hist_, title_x=None, title_y=None, draw_option=None, range_y=(None, None), pad_name=None, dont_change=False, uncert_band=None, redraw=None):

    if settings._book_mode.lower() == "start" and not settings._same_canvas:
      settings.set_same_canvas(True)
      self.already_drawn = []
      self.getPad("pad0").cd()
      self.pads[self.getPad("pad0")] = []

    if not isinstance(hist_, list):
      hist_ = [hist_]
    self.addAxisTitle(hist_, title_x, title_y)

    # Clone histograms in case of further modifications
    hist = [o.Clone("%s_clone" % o.GetName()) for o in hist_]

    # Apply some aesthetic changes to histograms
    if not dont_change:
      helper.make_me_pretty(hist)

    # Get the pad on which to draw the histogram
    if not pad_name:
      ROOT.gPad.cd()
    else:
      if pad_name in self.getPadNames():
        self.getPad(pad_name).cd()
      else:
        print("[WARNING] A pad with the name `%s` does not exist." % pad_name)
        ROOT.gPad.cd()

    # Add this histo to this pad
    self.add2pad(hist)    
    # Sort
    histos = filter(lambda x : utils.check.inherits_from(x, settings.check_obj) and x.GetName() not in self.already_drawn, self.pads[self.getPad()])

    # Adjust range
    if range_y[0] != None and range_y[1] != None:
      helper.adjust_y(histos, range_y[0], range_y[1])
      self.custom_range_y = range_y
    elif settings.auto.adjust_y:
      helper.adjust_y(histos)
    # Draw the histogram/graph
    helper.draw_histos(histos, draw_option if draw_option != None else self.draw_option, len(self.already_drawn) != 0, n_hist_drawn = len(self.already_drawn), uncert_band=uncert_band, redraw=redraw)
    [self.already_drawn.append(h.GetName() )for h in hist]

    # If this histogram already exists rename
    # TODO

    # Update
    ROOT.gPad.Update()

    # Check if common text has been set
    #if settings._common_text != None and not self.common_text_added:
    if settings._common_text != None:
      settings._common_text.draw()
      self.common_text_added = True
    #if settings._common_head != None and not self.common_head_added: 
    if settings._common_head != None: 
      settings._common_head.draw()
      self.common_head_added = True

    return self 


  def addHist1D (self, hist, title_x=None, title_y=None, draw_option=settings.histo.draw_option, range_y=(settings.histo.ymin, settings.histo.ymax), pad_name=None):

    return self.addHist(hist, title_x=title_x, title_y=title_y, draw_option=draw_option, range_y=range_y , pad_name=pad_name)


  def addHist2D (self, hist, title_x=None, title_y=None, draw_option=settings.histo.draw_option, range_y=(settings.histo.ymin, settings.histo.ymax) , pad_name=None):

    return self.addHist(hist, title_x=title_x, title_y=title_y, draw_option=draw_option, range_y=range_y , pad_name=pad_name)


  def addGraph (self, hist, title_x=None, title_y=None, draw_option=settings.graph.draw_option, range_y=(settings.histo.ymin, settings.histo.ymax) , pad_name=None):

    return self.addHist(hist, title_x=title_x, title_y=title_y, draw_option=draw_option, range_y=range_y, pad_name=pad_name)


  def addLegend (self, xmin=None, ymin=None, xmax=None, ymax=None, entries=None, title=None, n_cols=1, add_entries=[]):

    if not isinstance(add_entries, list): add_entries = [add_entries]

    # Check defaults
    if settings.legend.x and (settings.legend.x[0] != settings.legend.x1):
      settings.legend.x1 = settings.legend.x[0]
    xmin = settings.legend.x1 if xmin is None else xmin
    if settings.legend.y and (settings.legend.y[0] != settings.legend.y1):
      settings.legend.y1 = settings.legend.y[0]
    ymin = settings.legend.y1 if ymin is None else ymin
    if settings.legend.x and (settings.legend.x[1] != settings.legend.x2):
      settings.legend.x2 = settings.legend.x[1]
    xmax = settings.legend.x2 if xmax is None else xmax
    if settings.legend.y and (settings.legend.y[1] != settings.legend.y2):
      settings.legend.y2 = settings.legend.y[1]
    ymax = settings.legend.y2 if ymax is None else ymax

    # Check entries
    if entries is not None:
      if all(isinstance(entry, str) for entry in entries):
        # Get all histogras in current pad
        histos = filter(lambda x : utils.check.inherits_from(x, settings.check_obj), self.pads[self.getPad()])
        n = min(len(histos), len(entries))
        entries = [ (histos[i], entries[i]) for i in range(n) if histos[i].GetTitle() != "dummy" and "dummy" not in x.GetName()]
    else:
        entries = filter(lambda x : utils.check.inherits_from(x, settings.check_obj) and x.GetTitle()!="dummy" and "dummy" not in x.GetName(), self.pads[self.getPad()])

    if add_entries:
      for h in add_entries:
        entries.append(h)

    # Get legend
    leg = helper.add_legend(xmin, ymin, xmax, ymax, entry=entries, title=title, n_cols=n_cols)
    leg.Draw()
    scale = ROOT.gPad.GetWNDC()*ROOT.gPad.GetHNDC()
    leg.SetTextSize(leg.GetTextSize() / scale)

    # Add this legend to this pad
    self.add2pad(leg)

    return self 


  def addTextFrame(self, xmin=None, ymin=None, xmax=None, ymax=None, align="left"):

    txt_frame = helper.textFrame(xmin, ymin, xmax, ymax, align=align)
    self.add2pad(txt_frame)
    return txt_frame


  def addTextHeader(self, text=""):

    if isinstance(text, str):
      txt_header = helper.textHeader(text)
      self.add2pad(txt_header)
      return txt_header
    elif isinstance(text, list):
      txt_header = helper.textHeader(text[0])
      self.add2pad(txt_header)
      for i in range(1, len(text)):
        txt_header.addText(text[i])
      return self

 
  def addTimeStamp(self, xmin=settings.timestamp.xmin, ymin=settings.timestamp.ymin):
  
    txt = helper.add_time_stamp()
    self.add2pad(txt)
    return self 




  def addRelError (self, hist, title="Ratio", draw_option="", add2pad=None, range_y=(None, None), n_div=None, y=None, logx=False, uncert_band=None, limit_y=None):

    # Get the pad to which to add the ratio
    pad = self.getPad("pad0")

    histos = filter(lambda x : utils.check.inherits_from(x, settings.check_obj), self.pads[self.getPad()])

    for h in histos:
      h.GetXaxis().SetLabelOffset(-999)

    # Get the dimensions of the MAIN pad
    ROOT.gPad.Update()
    self.Update()
    xmin, ymin = pad.GetXlowNDC(), pad.GetYlowNDC()
    xmax, _ymax = xmin + pad.GetWNDC(), ymin + pad.GetHNDC()

    ymax_new = 1

    # Check if there is already a ratio pad
    if any(["ratio" in name for name in self.getPadNames()]):

      # Update margins
      # Get sizea of ratio pad and update pad
      delta_y = settings.ratio.dy * (_ymax - ymin )
      ymax_new = 1.3*(delta_y+settings.ratio.separation)
      pad.SetBottomMargin(0.50)
      scale = pad.GetWNDC()*pad.GetHNDC()

      # Get name
      pad_name_ratio = [name for name in self.getPadNames() if "ratio" in name][0]
      # Get Ratio pad
      pad_ratio = self.getPad(pad_name_ratio)
      # Get the dimensions of the RATIO pad
      ROOT.gPad.Update()
      self.Update()
      height_ratio = pad_ratio.GetHNDC()
      xmin_ratio, ymin_ratio = pad_ratio.GetXlowNDC(), pad_ratio.GetYlowNDC()
      xmax_ratio, _ymax_ratio = xmin_ratio + pad_ratio.GetWNDC(), ymin_ratio + pad_ratio.GetHNDC()
      pad_ratio.SetPad(xmin_ratio, 0.2, xmax_ratio, 0.5)
      pad_ratio.SetTopMargin(0.08)
      pad_ratio.SetBottomMargin(0.46)
#      pad_ratio.SetBottomMargin(0.4)
      #pad_ratio.RedrawAxis()
      #for h in [hist_num, hist_denum]:
      histos_ratio = filter(lambda x : utils.check.inherits_from(x, settings.check_obj), self.pads[pad_ratio])
#      scale = pad_ratio.GetWNDC()*pad_ratio.GetHNDC()
      for h in histos_ratio:
        h.GetXaxis().SetLabelOffset(-999)
        h.GetXaxis().SetTitleOffset(-999)

    else:
      # Get sizea of ratio pad and update pad
      delta_y = settings.ratio.dy * (_ymax - ymin )
      pad.SetBottomMargin(delta_y+settings.ratio.separation)
      ymax_new = delta_y+settings.ratio.separation

    #for h in [hist_num, hist_denum]:
    for h in hist:
      h.GetXaxis().SetLabelSize(h.GetXaxis().GetLabelSize()*1.1)
#      h.GetXaxis().SetTitle(title_x)
#      h.GetXaxis().SetTitleOffset(-999)

    if settings._book_mode.lower() == "start":
      for key in self.pads:
        if "ratio" in key.GetName():
          del self.pads[key]

    pad.cd()
    # Add new pad
    self.addPad("%s_relError" % pad.GetName(), 0, 0, 1, 0.75, pad_name="pad0")
    pad_new = self.getPad("%s_relError" % pad.GetName())
    pad_new.cd()

    for i_hist, h in enumerate(hist):
      # Remove labels and title (will be displayed in ratio)

      # Set Ranges
      if settings.ratio.ymin != None and settings.ratio.ymax != None:
        h.GetYaxis().SetRangeUser(settings.ratio.ymin, settings.ratio.ymax)

      # Ratio histogram belongs to ratio pad
      self.add2pad(h)

      # Set some properties
      h.SetLineStyle(1)
      h.SetLineStyle(1)

      # Set Range
      h.GetYaxis().SetRangeUser(hist[0].GetMinimum(), hist[0].GetMaximum())

      if i_hist == 0:
        h.Draw(draw_option)
      else:
        h.Draw(draw_option+" SAME")

    # Adjust range
    if limit_y is None:
      if range_y[0] != None and range_y[1] != None:
        helper.adjust_y(hist, range_y[0], range_y[1])
      elif settings.auto.adjust_y:
        helper.adjust_y(hist)
    else:
      for h in hist:
        if limit_y[0] > h.GetMinimum():
          h.SetMinimum(limit_y[0])
        if limit_y[1] > h.GetMaximum():
          h.SetMaximum(limit_y[1])

#      for h in hist:
#        if h.GetMinimum() < -1: h.SetMinimum(-1)
#  
#      for h in hist:
#        if h.GetMaximum() > 1: h.SetMaximum(1)

    # Draw the histogram/graph
#    hist[0].GetXaxis().SetLabelSize(histos[0].GetXaxis().GetLabelSize()/scale)  
#    hist[0].GetYaxis().SetLabelSize(histos[0].GetYaxis().GetLabelSize()/scale)  
#    hist[0].GetXaxis().SetTitleSize(histos[0].GetXaxis().GetTitleSize()/scale)  
#    hist[0].GetXaxis().SetTitleOffset(hist[0].GetXaxis().GetTitleOffset()+0.5)
#    hist[0].GetYaxis().SetTitleSize(histos[0].GetYaxis().GetTitleSize()/scale)  
#    hist[0].GetYaxis().SetTitleOffset(histos[0].GetYaxis().GetTitleOffset()*scale)
    if settings.ratio.center_title_y: 
      hist[0].GetYaxis().CenterTitle()

    # Update Margin
    pad_new.SetBottomMargin(0.12)
    pad_new.SetTopMargin(0.58)

    # Set Pad range
    pad_new.Range(hist[0].GetXaxis().GetXmin(), hist[0].GetMinimum(), hist[0].GetXaxis().GetXmax(), hist[0].GetMaximum())
#
    # Set title?
    if title != "":
      hist[0].GetYaxis().SetTitle(title)
    hist[0].GetXaxis().SetTitle(hist[0].GetXaxis().GetTitle())

    if not n_div is None:
      hist[0].SetNdivisions(n_div, "Y")

    hist[0].Draw("AXIS SAME")

    # Add a line on top?
    ROOT.gPad.Update()
#    line = ROOT.TLine(hist[0].GetXaxis().GetXmin(), settings.ratio.yintersection, hist[0].GetXaxis().GetXmax(), settings.ratio.yintersection)
#    line.SetLineStyle(settings.ratio.line_style)
#    line.SetLineColor(settings.ratio.line_color)
#    line.SetLineWidth(settings.ratio.line_width)
#    self.add2pad(line)
#    line.Draw()
#
    if logx:
      ROOT.gPad.SetLogx()

    return self


  def addRatio (self, hist_num=None, hist_denum=None, hist_ratio=None, title="Ratio", draw_option="", add2pad=None, range_y=(None, None), n_div=None, y=None, logx=False, uncert_band=None):

    # Do some checks
    if not hist_num and not hist_denum and not hist_ratio:
      return self

    # Get the pad to which to add the ratio
    pad = self.getPad(add2pad)

    # Get the dimensions of the pad
    ROOT.gPad.Update()
    self.Update()
    xmin, ymin = pad.GetXlowNDC(), pad.GetYlowNDC()
    xmax, _ymax = xmin + pad.GetWNDC(), ymin + pad.GetHNDC()

    # Get size of ratio pad and update pad
    delta_y = settings.ratio.dy * (_ymax - ymin )
    pad.SetBottomMargin(delta_y+settings.ratio.separation)

    histos = filter(lambda x : utils.check.inherits_from(x, settings.check_obj), self.pads[self.getPad()])

    h_denum = None

    only_one_denum = False
    hist_ratios = []
    if hist_ratio == None and (hist_num != None and hist_denum != None):
      # Check if histograms or histogram names
      if isinstance(hist_num, str) and isinstance(hist_denum, str):
        # Get histograms/graphs from list
        hist_num = [o for o in histos if o.GetName()==hist_num or o.GetName()=="%s_clone" % hist_num]
        h_denum = [o for o in histos if o.GetName()==hist_denum or o.GetName()=="%s_clone" % hist_denum][0].Clone()
        only_one_denum = True
      elif isinstance(hist_num, list) and isinstance(hist_denum, str):
        # Get histograms/graphs from list
        hist_num = [o for o in histos for o_ in hist_num if o.GetName()==o_.GetName() or o.GetName()=="%s_clone" % o_.GetName()]
        h_denum = [o for o in histos if o.GetName()==hist_denum or o.GetName()=="%s_clone" % hist_denum][0].Clone()
        only_one_denum = True
      elif isinstance(hist_num, list) and ("TH1" in type(hist_denum).__name__):
        # Get histograms/graphs from list
        hist_num = [o for o in histos for o_ in hist_num if o.GetName()==o_.GetName() or o.GetName()=="%s_clone" % o_.GetName()]
        h_denum = [o for o in histos if o.GetName()==hist_denum.GetName() or o.GetName()=="%s_clone" % hist_denum]
        only_one_denum = True
        if len(h_denum) == 0:
          h_denum = hist_denum.Clone()
        else:
          h_denum = h_denum[0].Clone()
      elif isinstance(hist_num, list) and isinstance(hist_denum, list):
        hist_num = [o for o in histos for o_ in hist_num if o.GetName()==o_.GetName() or o.GetName()=="%s_clone" % o_.GetName()]
        h_denum = [o for o in histos for o_ in hist_denum if o.GetName()==o_.GetName() or o.GetName()=="%s_clone" % o_.GetName()]
        only_one_denum = False



      if only_one_denum:
        # Add offset to make sure that we dont divide by zero
        for i in range(h_denum.GetNbinsX()):
          if h_denum.GetBinContent(i+1) == 0:
            h_denum.SetBinContent(i+1, h_denum.GetBinContent(i+1) + 1E-10)
        for h in hist_num:
          # Get ratio plot
          hist_ratios.append(h.Clone("%s_ratio" % h.GetName()))
          hist_ratios[-1].Divide(h_denum)
          if "ugly" not in hist_ratios[-1].GetName():
            hist_ratios[-1].SetLineStyle(1)
          hist_ratios[-1].SetLineWidth(2)
          hist_ratios[-1].GetXaxis().SetTitle(histos[0].GetXaxis().GetTitle())
          hist_ratios[-1].SetDirectory(0)
          for h_ref in histos:
            if h_ref.GetName().replace("_clone", "") in hist_ratios[-1].GetName():
              hist_ratios[-1].SetLineColor(h_ref.GetLineColor())
      else:
        for h1, h2 in zip(hist_num, h_denum):
          # Add offset to make sure that we dont divide by zero
          for i in range(h2.GetNbinsX()):
            if h2.GetBinContent(i+1) == 0:
              h2.SetBinContent(i+1, h2.GetBinContent(i+1) + 1E-10)
          # Get ratio plot
          hist_ratios.append(h1.Clone("%s_ratio" % h1.GetName()))
          hist_ratios[-1].Divide(h2)
          if "ugly" not in hist_ratios[-1].GetName():
            hist_ratios[-1].SetLineStyle(1)
          hist_ratios[-1].SetLineWidth(2)
          hist_ratios[-1].GetXaxis().SetTitle(histos[0].GetXaxis().GetTitle())
          hist_ratios[-1].SetDirectory(0)
          for h_ref in histos:
            if h_ref.GetName().replace("_clone", "") in hist_ratios[-1].GetName():
              hist_ratios[-1].SetLineColor(h_ref.GetLineColor())

    #for h in [hist_num, hist_denum]:
    for h in histos:
      h.GetXaxis().SetLabelOffset(-999)
      h.GetXaxis().SetTitleOffset(-999)

    if settings._book_mode.lower() == "start":
      for key in self.pads:
        if "ratio" in key.GetName():
          del self.pads[key]

    # Add new pad
    self.addPad("%s_ratio" % pad.GetName(), 0, 0, 1, delta_y, pad_name=add2pad)
    pad_ratio = self.getPad("%s_ratio" % pad.GetName())

    # Adjust range
    if range_y[0] != None and range_y[1] != None:
      helper.adjust_y(hist_ratios, range_y[0], range_y[1])
    elif settings.auto.adjust_y:
      helper.adjust_y(hist_ratios)

    for i_hist, hist in enumerate(hist_ratios):
      # Remove labels and title (will be displayed in ratio)

      # Set Ranges
      if settings.ratio.ymin != None and settings.ratio.ymax != None:
        hist.GetYaxis().SetRangeUser(settings.ratio.ymin, settings.ratio.ymax)

      # Ratio histogram belongs to ratio pad
      self.add2pad(hist)

      # Set Range
      hist.GetYaxis().SetRangeUser(hist_ratios[0].GetMinimum(), hist_ratios[0].GetMaximum())

      if i_hist == 0:
        # Add uncertainty band
        if uncert_band is not None:
          tmp_list = uncert_band[:]
          for h in tmp_list:
            h_1 = h.Clone()
            h_1.Divide(h_1)
            h.Add(h_1)
          hist.Draw(draw_option)
#          tmp_list.sort(key=lambda x: x.Integral(), reverse=True)
          tmp_list[0].GetYaxis().SetRangeUser(hist_ratios[0].GetMinimum(), hist_ratios[0].GetMaximum())
          tmp_list[1].GetYaxis().SetRangeUser(hist_ratios[0].GetMinimum(), hist_ratios[0].GetMaximum())
          # Draw stuff
          tmp_list[1].SetFillColorAlpha(settings.error.fill_color, settings.error.fill_color_alpha)
          tmp_list[1].SetLineWidth(settings.error.line_width)
          tmp_list[1].SetFillStyle(settings.error.fill_style)
          tmp_list[1].Draw("HIST SAME")
          tmp_list[0].SetFillColorAlpha(0, 1)
          tmp_list[0].SetFillStyle(1001)
          tmp_list[0].SetLineWidth(0)
          tmp_list[0].Draw("HIST SAME")
          hist.Draw("AXIS SAME")
          self.add2pad(tmp_list[0])
          self.add2pad(tmp_list[1])
        else:
          hist.Draw(draw_option)
      else:
        hist.Draw(draw_option+" SAME")

    # Draw the histogram/graph
    scale = pad_ratio.GetWNDC()*pad_ratio.GetHNDC()
    hist_ratios[0].GetXaxis().SetLabelSize(histos[0].GetXaxis().GetLabelSize()/scale)  
    hist_ratios[0].GetYaxis().SetLabelSize(histos[0].GetYaxis().GetLabelSize()/scale)  
    hist_ratios[0].GetXaxis().SetTitleSize(histos[0].GetXaxis().GetTitleSize()/scale)  
    hist_ratios[0].GetXaxis().SetTitleOffset(hist_ratios[0].GetXaxis().GetTitleOffset()+0.5)
    hist_ratios[0].GetYaxis().SetTitleSize(histos[0].GetYaxis().GetTitleSize()/scale)  
    hist_ratios[0].GetYaxis().SetTitleOffset(histos[0].GetYaxis().GetTitleOffset()*scale)
    if settings.ratio.center_title_y: 
      hist_ratios[0].GetYaxis().CenterTitle()

    # Update Margin
    pad_ratio.SetBottomMargin(settings.ratio.bottom_margin/scale)

    # Set Pad range
    pad_ratio.Range(hist_ratios[0].GetXaxis().GetXmin(), hist_ratios[0].GetMinimum(), hist_ratios[0].GetXaxis().GetXmax(), hist_ratios[0].GetMaximum())
#
    # Set title?
    if title != "":
      hist_ratios[0].GetYaxis().SetTitle(title)
    hist_ratios[0].GetXaxis().SetTitle(hist_ratios[0].GetXaxis().GetTitle())

    if not n_div is None:
      hist_ratios[0].SetNdivisions(n_div, "Y")

    # Add a line on top?
    ROOT.gPad.Update()
    line = ROOT.TLine(hist_ratios[0].GetXaxis().GetXmin(), settings.ratio.yintersection, hist_ratios[0].GetXaxis().GetXmax(), settings.ratio.yintersection)
    line.SetLineStyle(settings.ratio.line_style)
    line.SetLineColorAlpha(settings.ratio.line_color, settings.ratio.line_color_alpha)
    line.SetLineWidth(settings.ratio.line_width)
    self.add2pad(line)
    line.Draw()

#    # Adjust range
#    if range_y[0] != None and range_y[1] != None:
#      helper.adjust_y(hist_ratios, range_y[0], range_y[1])
#    elif settings.auto.adjust_y:
#      helper.adjust_y(hist_ratios)

    if logx:
      ROOT.gPad.SetLogx()

    return self


  def close (self):

    for pad in self.pads:
      for o in self.pads[pad]:
        o.Delete()
      pad.Modified()
      pad.Update()
    self.Modified()
    self.Update()
    self.Close()
    self.Delete()
    ROOT.gSystem.ProcessEvents()
    ROOT.gROOT.Reset()


  def closeBook (self):

    settings.set_book_mode(mode="close")
    return self


  def fromFile(self, f, onames, mode="strict", title_x=None, title_y=None, title=None, pad_name=None, draw_option=None):

    import myroot.fio

    # Get file with object
    f = _fname_or_file(f, self.tfiles)

    # Some other checks
    if not isinstance(onames, list): onames = [onames]

    # Container for objects retrieved from ROOT file
    objs = []

    # Get objects from file
    for i, oname in enumerate(onames):
      objs.append(myroot.fio.filter_by_name(f, oname, mode))
      if title != None:
        if isinstance(title, str):
          objs[-1].SetTitle(title)
        elif isinstance(title, list) and len(title)==len(onames):
          objs[-1].SetTitle(title[i])

    self.tfiles.append(f)
    # TODO: Fix this!
    ROOT.gPad.SetFillColor(4000) 
    # Add to histograms
    self.addHist(objs, title_x=title_x, title_y=title_y, draw_option=draw_option, pad_name=pad_name)

  #  ROOT.gPad.SetCanvasSize(settings.canvas.defw, settings.canvas.defh)
  #  ROOT.gPad.SetPad(-0.06, -0.06, 1.0, 1.06)

    return self


  def hist1DFromFile(self, f, tree_name, branch_name, x_range, range_y=(None, None), title_x=None, title_y=None, title=None, selection="", pad_name=None, draw_option=settings.histo.draw_option, normalize=False):

    if ".hdf5" in f or ".h5" in f:
      self.hist1DFromHDF5File(f, branch_name, x_range, group=tree_name, range_y=range_y, title_x=title_x, title_y=title_y, selection=selection, pad_name=pad_name, draw_option=settings.histo.draw_option, normalize=normalize, title=title, name=None)
    else:
      self.hist1DFromROOTFile(f, tree_name, branch_name, x_range, range_y=range_y, title_x=title_x, title_y=title_y, title=title, selection=selection, pad_name=pad_name, draw_option=settings.histo.draw_option)

    return self


  def objFromFile(self, f, name, range_y=(None, None), title_x=None, title_y=None, title=None, selection="", pad_name=None, draw_option=settings.histo.draw_option, normalize=False):

    if ".hdf5" in f or ".h5" in f:
      print("[INFO] NOT IMPLEMENTED YET.")
    else:
      self.objFromROOTFile(f, name, range_y=range_y, title_x=title_x, title_y=title_y, title=title, pad_name=pad_name, draw_option=draw_option)

    return self



  def hist1DFromROOTFile(self, f, tree_name, branch_name, x_range, range_y=(None, None), title_x=None, title_y=None, title=None, selection="", pad_name=None, draw_option=settings.histo.draw_option):

    import myroot.reader

    f = _fname_or_file(f, self.tfiles)

    obj = myroot.reader.get_h1_from_file(f, tree_name, branch_name, x_range, selection=selection, name=branch_name)
    if title: obj.SetTitle(title)

    # Add to histograms
    self.addHist(obj, title_x=title_x, title_y=title_y, draw_option=draw_option, pad_name=pad_name, range_y=range_y)
    
    return self


  def objFromROOTFile(self, f, name, range_y=(None, None), title_x=None, title_y=None, title=None, pad_name=None, draw_option=settings.histo.draw_option):

    import myroot.fio

    if isinstance(f, list) and not isinstance(name, list):
      for file in f:
        f = _fname_or_file(f, self.tfiles)
        obj = myroot.fio.filter_by_name(f, name)
        obj.SetDirectory(0)
        if title: obj.SetTitle(title)
        self.addHist(obj, title_x=title_x, title_y=title_y, draw_option=draw_option, pad_name=pad_name, range_y=range_y)
    elif not isinstance(f, list) and isinstance(name, list):
      f = _fname_or_file(f, self.tfiles)
      for n in name:
        obj = myroot.fio.filter_by_name(f, n)
        if title: obj.SetTitle(title)
        self.addHist(obj, title_x=title_x, title_y=title_y, draw_option=draw_option, pad_name=pad_name, range_y=range_y)
    else:
      f = _fname_or_file(f, self.tfiles)
      obj = myroot.fio.filter_by_name(f, name)
      obj.SetDirectory(0)
      if title: obj.SetTitle(title)
      self.addHist(obj, title_x=title_x, title_y=title_y, draw_option=draw_option, pad_name=pad_name, range_y=range_y)
    
    return self


  def hist1DFromHDF5File(self, f, branch_name, x_range, group=None, range_y=(None, None), title_x=None, title_y=None, title=None, selection="", pad_name=None, draw_option=settings.histo.draw_option, normalize=False, name=None,):

    import myroot.cut
    import myroot.reader
    import myutils.h5

    # name of the dataset (take groups into acount)
    ds_name = branch_name
    if group is not None: ds_name = os.path.join(group, ds_name)

    # Read array from HDF5 file
    arr = myutils.h5.load_hdf5(f, name=ds_name)
    arr = myroot.cut.rootlike_selection(arr, branches=branch_name, selection=selection)

    # Get histogram
    nbins, xmin, xmax = x_range.split(",")
    x_range = (float(xmin), float(xmax))
    obj = myroot.reader.get_h1_from_array(arr, nbins=int(nbins), range_x=range_x, title_x=title_x, title_y=title_y, normalize=normalize, name=name, weights=[])
    if title: obj.SetTitle(title)

    # Add to histograms
    self.addHist(obj, title_x=title_x, title_y=title_y, draw_option=draw_option, pad_name=pad_name, range_y=range_y)
    
    return self


  def hist1DFromArray(self, arr, nbins=None, range_x=(None, None), range_y=(None, None), title_x="", title_y="", normalize=False, name=None, title="", weights=[], pad_name=None, draw_option=settings.histo.draw_option):

    import myroot.reader
    obj = myroot.reader.get_h1_from_array(arr, nbins=nbins, range_x=range_x, title_x=title_x, title_y=title_y, normalize=normalize, name=name, title=title, weights=weights)
    if title: obj.SetTitle(title)

    # Add to histograms
    self.addHist(obj, title_x=title_x, title_y=title_y, draw_option=draw_option, pad_name=pad_name, range_y=range_y)
    
    return self


  def hist2DFromArray(self, arr, nbins_x=None, range_x=(None, None), nbins_y=None, range_y=(None, None), title_x="", title_y="", normalize=False, name=None, title="", weights=[], pad_name=None, draw_option=settings.histo.draw_option):

    import myroot.reader
    obj = myroot.reader.get_h2_from_array(arr, nbins_x=nbins_x, range_x=range_x, nbins_y=nbins_y, range_y=range_y, title_x=title_x, title_y=title_y, normalize=normalize, name=name, title=title, weights=weights)

    # Add to histograms
    self.addHist(obj, title_x=title_x, title_y=title_y, draw_option=draw_option, pad_name=pad_name)
    
    return self


  def scatter1DFromFile(self, f, tree_name, x_branch_name, y_branch_name, x_range, y_range, name="t_scatter", title_x=None, title_y=None, title=None, selection="", pad_name=None, draw_option="", marker_style=None, n=None):

    import myroot.reader

    f = _fname_or_file(f, self.tfiles)

    obj = myroot.reader.get_h2_from_file(f, tree_name, x_branch_name, y_branch_name, x_range, y_range, selection=selection, h_name=name, n=n)

    if title:
      obj.SetTitle(title)
    if marker_style != None:
      obj.SetMarkerStyle(marker_style)

    # Add to histograms
    self.addHist(obj, title_x=title_x, title_y=title_y, draw_option=draw_option, pad_name=pad_name)
    return self


  def setFillStyle(self, fill_style=1):

    settings.histo.fill_style = fill_style
    return self


  def setFillColor(self, fill_color=1, alpha=1):

    settings.histo.fill_color = fill_color
    settings.histo.fill_color_alpha = alpha
    return self


  def setLineStyle(self, line_style=1):

    settings.histo.line_style = line_style
    return self


  def setLineColor(self, line_color=1):

    settings.histo.line_color = line_color
    return self


  def setMarkerStyle(self, line_color=20):

    settings.histo.marker_style = line_color
    return self


  def setLineWidth(self, line_width=1):

    settings.histo.line_width = line_width
    return self


  def setPath(self, path):

    self.path = path
    # Create the directory
    if self.path: utils.utils.mkdir(self.path)
    return self


  def setRangeX(self, xmin, xmax):

    histos = filter(lambda x : utils.check.inherits_from(x, settings.check_obj), self.pads[self.getPad()])
    for o in histos:
      if "TGraph" in type(o).__name__:
        o.GetXaxis().SetLimits(xmin, xmax)
      o.GetXaxis().SetRangeUser(xmin, xmax)
    return self


  def setRangeY(self, ymin=None, ymax=None, set_zero=False):

    histos = filter(lambda x : utils.check.inherits_from(x, settings.check_obj), self.pads[self.getPad()])
    if set_zero:
      y_min, y_max = helper.adjust_y(histos, ymin, ymax)
      yy_min = min([h.GetMinimum() for h in histos])
      diff = abs(max([h.GetMaximum() for h in histos]) - yy_min)
#      diff=y_max-y_min
      for o in histos:
        o.GetYaxis().SetRangeUser(-diff*0.02, y_max)
    elif ymin != None and ymax != None:
      for o in histos:
        o.GetYaxis().SetRangeUser(ymin, ymax)
    else:
      helper.adjust_y(histos, ymin, ymax)
    return self 


  def setRangeZ(self, zmin=None, zmax=None):

    histos = filter(lambda x : utils.check.inherits_from(x, settings.check_obj), self.pads[self.getPad()])
    for o in histos:
      o.GetZaxis().SetRangeUser(zmin, zmax)
    return self 


  def setMaxDigits (self, n=2):

    ROOT.TGaxis.SetMaxDigits(int(n))
    return self


  def setDrawOption(self, option):

    self.draw_option = option
    return self


  def setPadMargin (self, margin=None, side="right"):

    if not isinstance(margin, list): margin = [margin]
    if not isinstance(side, list): side = [side]
    if len(side) > len(margin):
      while len(side) > len(margin):
        margin.append(margin[0])

    for side, margin in zip(side, margin):
      if side.lower() == "right":
        ww, wh = self.GetWw(), self.GetWh()
        if margin is None:
          m = (ww - wh) / float(ww)
        else:
          m = margin
        ROOT.gPad.SetRightMargin(m)
      if side.lower() == "top":
        ww, wh = self.GetWw(), self.GetWh()
        if margin is None:
          m = (ww - wh) / float(ww)
        else:
          m = margin
        ROOT.gPad.SetTopMargin(m)
      if side.lower() == "bottom":
        ww, wh = self.GetWw(), self.GetWh()
        if margin is None:
          m = (ww - wh) / float(ww)
        else:
          m = margin
        ROOT.gPad.SetBottomMargin(m)

 #   setattr(ROOT.gPad, "Set%sMargin" % side.title(), margin)
    ROOT.gPad.Update()
    return self


  def setColorPalette(self, option=ROOT.kWaterMelon, invert=False):
#  def setColorPalette(self, option=87, invert=False):


    if isinstance(option, int) or type(option).__name__ == "long":
      if self.color_palette != option:
        ROOT.gStyle.SetPalette(option)
        self.color_palette = option
    if isinstance(option, str):
      import styles
      if "malta" in option.lower():
        styles.MaltaStyle()

#    if not invert and is_version18():
#      ROOT.TColor.InvertPalette()
#      Figure.color_palette_inv = True
    return self


  def save(self, fname=settings.file.output, tex=settings.file.save_tex):

    fname = os.path.join(self.path, self.fname) if fname == None else fname
    helper.save_canvas(self, fname, tex)
    return self 


  def dumpToROOT(self, fname="dat/output.root", write_mode="update", verbose=True):

    if not os.path.exists(os.path.dirname(fname)):
      os.makedirs(os.path.dirname(fname))

    # Check if this file exists
    if fname not in [f.GetName() for f in self.files]:
      self.addFile(fname, write_mode=write_mode, verbose=verbose)

    # Add all objects to file
    for pad in self.pads:
      obj = filter(lambda x : utils.check.inherits_from(x, settings.check_obj), self.pads[pad]) 
      for o in obj:
        o_clone = o.Clone()
        o.SetLineColor(settings.histo.line_color)
        o.SetMarkerColor(settings.histo.marker_color)
        self.addObj(o, fname, directory="hist", verbose=verbose)
    # Also save full canvas
    self.addObj(self.Clone(), fname, directory="canv", verbose=verbose)
    self.flush()
    self.current_file.Close()
    
    return self


  def setLog(self, x=False, y=True, range_y=(None, None)):

    ROOT.gPad.SetLogx(x)
    ROOT.gPad.SetLogy(y)
    histos = filter(lambda x : utils.check.inherits_from(x, settings.check_obj), self.pads[self.getPad()])
    for h in histos:
      h.SetMinimum(1e-20)

    if len(histos) == 0:
      return
    if self.custom_range_y != None and range_y[0] == None and range_y[1] == None and settings.auto.adjust_y:
      helper.adjust_y(histos, self.custom_range_y[0], self.custom_range_y[1])
    elif range_y[0] != None and range_y[1] != None:
      helper.adjust_y(histos, range_y[0], range_y[1])
    elif  settings.auto.adjust_y:
      helper.adjust_y(histos)
    return self


  def setMarkers (self, markers):

    if not isinstance(markers, list): markers = [markers]
    settings.histo.marker_list = markers
    return self


  def setLogX(self, use=True):

    return self.setLog(x=use, y=False)


  def setLogY(self, use=True, range_y=(None, None)):

    histos = filter(lambda x : utils.check.inherits_from(x, settings.check_obj), self.pads[self.getPad()])
    for h in histos: h.GetYaxis().SetRangeUser(1.e-40, h.GetMaximum())
    return self.setLog(x=False, y=use, range_y=range_y)


  def setLogZ(self, use=True, range_y=(None, None)):

    ROOT.gPad.SetLogz()
    return self


  def startBook (self):

    settings.set_book_mode (mode="start")
    return self


  def setSubPad(self, pad_no):

    Canvas.cd(self, pad_no)
    return self


  def useAutoLog(self):

    histos = filter(lambda x : utils.check.inherits_from(x, settings.check_obj), self.pads[self.getPad()])
    helper.use_log(histos)
    return self 


  def normalize(self, area=1.0, range_y=(None, None)):

    # Get all histograms on current pad
    histos = filter(lambda x : utils.check.inherits_from(x, ("TH1", "TF1")), self.pads[self.getPad()])
    for h in histos:
      utils.utils.norm_h(h, norm=area)
      # Update title
      if "Events" in h.GetYaxis().GetTitle():
        self.addAxisTitle(h, title_x=h.GetXaxis().GetTitle())
 
    if self.custom_range_y != None and range_y[0] == None and range_y[1] == None and settings.auto.adjust_y:
      helper.adjust_y(histos, self.custom_range_y[0], self.custom_range_y[1])
    elif range_y[0] != None and range_y[1] != None:
      helper.adjust_y(histos, range_y[0], range_y[1])
    elif  settings.auto.adjust_y:
      helper.adjust_y(histos)

    return self
    

# Test
if __name__ == "__main__":

  import random

  print("[Start test]")  
  # Create object   
  ha = ROOT.TH1F("A", "Title for A,,l", 30, 0, 1)
  hb = ROOT.TH1F("B", "B", 30, 0, 1)
  hc = ROOT.TH1F("C", "C", 30, 0, 1)
  tF = ROOT.TFile("Out.root", "RECREATE")
  for i in range(100000):
    ha.Fill(random.random())
    hb.Fill(random.random())
    hc.Fill(random.random())
 #   h2.Fill(random.random(), random.random())
  ha.Write()
  hb.Write()
  hc.Write()
  tF.Close()
####  fig = Figure()
####  fig.addHist([ha, hb], title_x="Test [GeV]", pad_name="pad0")
####  fig.addLegend(title="TEST")
####  fig.addTextFrame().addExperiment().addText("Row 1").addText("Row 2")
####  fig.addTextHeader().addExperiment(text=", XXXXXXX")
####  fig.addTimeStamp()
####  fig.useAutoLog()
#  fig.addVaxis(0, 1)
#  fig.addHaxis(0, 1)
##  fig.addPad("pad1", 0.5, 0.5, 1.0, 1.0)
##  fig.addHist(hb, pad_name="pad1")
  # Add ratio
  #settings.ratio.range_y = (0.6, 1.4)
 ### settings.ratio.ymin = 0.6
 ### settings.ratio.ymax = 1.4
 ### fig.addRatio(ha, hb, add2pad="pad0")
 ### settings.ratio.reset()
 ### fig.addRatio(ha, hb, add2pad="pad1")
####  fig.save()


  Figure().fromFile("Out.root", ["A", "B"]) \
                 .addHist1D(hc) \
                 .addAxisTitle(title_x="Title [GeV]") \
                 .addLegend(title="DNN top tagger") \
                 .normalize() \
                 .addTimeStamp() \
                 .addRatio("A", "B") \
                 .addAxisTitle(title_y="A/B") \
                 .save("output2.pdf") \
                 .dumpToROOT("output_ratio.root")

  tG = ROOT.TGraph(5)
  tG.SetPoint(0,0,1)
  tG.SetPoint(1,1,2)
  tG.SetPoint(2,2,3)
  tG.SetPoint(3,3,4)
  tG.SetPoint(4,4,5)

  Figure().addGraph(tG, title_x="Title X", title_y="Title Y") \
          .setLog() \
          .setRangeX(2,3) \
          .save("output_graph.pdf") \
          .dumpToROOT()
  
  
