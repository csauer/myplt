import re
import config
import ROOT


debug = False
_common_text = None
_common_head = None
_book_mode = ""
_same_canvas = False

global_obj_containter = []

mpl_cmap = None

check_obj = ("TH1", "TF1", "TGraph", "TGraph2D", "TGraphAsymmErrors", "TH2", "THStack")

# Some global variables
_conf = config.ConfigReader()


def get_color (n, size):

  return n * int(ROOT.TColor.GetPalette().fN-1) // (size-1)


def set_same_canvas (mode):

  global _same_canvas
  _same_canvas = mode


def set_book_mode (mode):

  global _book_mode
  _book_mode = mode


def common_text ():

  global _common_text
  
  if _common_text == None:
    _common_text = helper.textFrame()

  return _common_text


def common_head ():

  global _common_head
  
  if _common_head == None:
    _common_head = helper.textHeader()

  return _common_head


def common_head_reset ():

  global _common_head
  _common_head = None


def common_reset ():

  global _common_text
  _common_text = None


def _reset (obj, title, idx=0):

  if not isinstance(title, list): title = [title]

  for att, value in _conf.getAttValue(only=title):

    if isinstance(value, str) \
       and len(value.split(_conf["Global"]["Delimiter"])) > 1 \
       and value != _conf["Global"]["Delimiter"]:
      value = value.split(_conf["Global"]["Delimiter"])[idx]

    setattr(obj, att, value)
    setattr(obj, att.replace("Set", "").lower(), value)
    setattr(obj, re.sub(r'(?<!^)(?=[A-Z])', '_', att.replace("Set", "")).lower(), value)
  

class monitor (object):

  @staticmethod
  def reset (): _reset(monitor, "Monitior")


class file (object):

  @staticmethod
  def reset (): _reset(file, "File")


class nameing (object):

  @staticmethod
  def reset (): _reset(nameing, "Nameing")


class histo(object):

  @staticmethod
  def reset (): 
    _reset(histo, "Hist")
    histo.legend.reset()

  class legend(object):
    @staticmethod
    def reset (): _reset(histo.legend, "Legend")


class legend(object):

  @staticmethod
  def reset (): _reset(legend, "Legend")


class function(object):

  @staticmethod
  def reset (): _reset(function, "Func")


class glob(object):

  @staticmethod
  def reset (): _reset(glob, "Gobal")


class textframe(object):

  @staticmethod
  def reset (): _reset(textframe, "TextFrame")


class textheader(object):

  @staticmethod
  def reset (): _reset(textheader, "TextHeader")


class timestamp(object):

  @staticmethod
  def reset (): _reset(timestamp, "TimeStamp")


class canvas (object):
  
  @staticmethod
  def reset (): _reset(canvas, "Canvas")


class pad (object):

  @staticmethod
  def reset (): _reset(pad, ["Pad", "Grid"])


class frame (object):

  @staticmethod
  def reset (): _reset(frame, "Frame")


class error (object):

  @staticmethod
  def reset (): _reset(error, "Error")


class ratio(object):

  @staticmethod
  def reset (): _reset(ratio, "Ratio")


class auto(object):

  @staticmethod
  def reset (): _reset(auto, "Auto")


class title(object):

  @staticmethod
  def reset (): _reset(title, "Title")


class graph(object):

  @staticmethod
  def reset (): _reset(graph, "Graph")


class opt(object):

  @staticmethod
  def reset (): _reset(opt, "Opt")


class axis (object):

  @staticmethod
  def reset ():
    axis.x.reset()
    axis.y.reset()
    axis.z.reset()
    _reset(axis, "Axis")

  class x (object):
    @staticmethod
    def reset (): _reset(axis.x, ["Title", "Label", "Axis"], idx=0)

  class y (object):
    @staticmethod
    def reset (): _reset(axis.y, ["Title", "Label", "Axis"], idx=1)
    
  class z (object):
    @staticmethod
    def reset (): _reset(axis.z, ["Title", "Label", "Axis"], idx=2)
 
   
# Apply all properties
_conf.apply()


error.reset()
# Ratio
ratio.reset()
monitor.reset()
nameing.reset()
file.reset()

# Histo
histo.reset()
pad.reset()
frame.reset()
canvas.reset()
function.reset()
glob.reset()
legend.reset()
textframe.reset()
textheader.reset()
timestamp.reset()
auto.reset()
title.reset()
graph.reset()
opt.reset()

# Axis
axis.reset()

import helper
