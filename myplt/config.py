import os, sys
import configparser
import unicodedata
import ROOT
script_path = os.path.dirname(__file__)


# Some hash tables
lookup_type = dict.fromkeys(["TH1", "TH2", "TF1", "TGraph"], "Hist")
[lookup_type.update(dict.fromkeys([key], key.replace("T", ""))) for key in ["TPad", "TCanvas", "TLegend"]]


# Some small helpers
def _represents_number (s, type=int):

  try: 
      type(s)
      return True
  except ValueError:
      return False


def _get_return (in_list, type):

  r_list = [type(item) for item in in_list]

  # Check if None are in there
  for i in range(len(r_list)):
    if _is_none(item): r_list[i] = None
  # Check if its a bool
  for i in range(len(r_list)):
    if _is_bool(item): r_list[i] = _get_bool(r_list[i])

  if len(r_list) == 1:
    return r_list[0]
  else:
    return r_list


def _is_dgit(x):

  try:
    float(x)
    return True
  except ValueError:
    return False


def _is_none(x):

  if x == "None":
    return True
  else:
    return False


def _is_bool(x):

  if x == True or x == "False" or x == "True" or x == "kTRUE" or x == "kFALSE":
    return True
  else:
    return False


def _get_bool (x):

  if x == 0: 
    return False
  if x == 1: 
    return True
  if x == "False":
    return False
  if x == "True":
    return True
  if x == "kTRUE":
    return ROOT.kTRUE
  if x == "kFALSE":
    return ROOT.kFALSE


class ConfigReader(configparser.ConfigParser):


  def __init__ (self, fname=os.path.join(script_path, "etc/style_default_root.ini"), delimiter=";"):

    super(ConfigReader, self).__init__()
    # Avoid conversion to lower case
    self.optionxform = str

    # Do some checks
    if not isinstance(fname, list): fname = [fname]
    # Set som einternal members
    self.delimiter = delimiter  

    # Add config files
    for f in fname:
      print("[INFO] Added plotting config file: `%s`" % f)
      self.read(f)

    # Apply settings
    self.apply()


  def apply (self, exclude=[], only=[]):

    for sec in self:
      if len(only) != 0:
        if sec not in only: continue
      elif len(exclude) != 0:
        if sec in exclude: continue
      for att in self[sec]:
        # Set color palette
        if sec == "Style":
          getattr(ROOT.gStyle, "Set%s" % att)(self.value(sec, att))
        elif hasattr(ROOT.gROOT, "Set%s" % att) and sec == "GROOT":
          getattr(ROOT.gROOT, "Set%s" % att)(self.value(sec, att))
        elif hasattr(ROOT.gStyle, "Set%s%s" % (sec, att)):
          # Get value(s)
          values = self.value(sec, att)
          if isinstance(values, list) and len(values) > 1:
            for i, axis in enumerate(["X", "Y", "Z"]):
              getattr(ROOT.gStyle, "Set%s%s" % (sec, att))(values[i], axis)
          else:
            getattr(ROOT.gStyle, "Set%s%s" % (sec, att))(values)


  def apply2obj (self, obj, exclude=[]):

    # Some quick checks
    if not isinstance(obj, list): obj = [obj]

    for o in obj:

      # Get name/type of object
      t_name = type(o).__name__

      # Get corresponding name
      base_name = [value for key, value in lookup_type.items() if key in t_name][0]

      # Check if this is a section of the configuration files
      if not base_name in self:
        print("[WARNING] The base name `%s` is not present in the config files. Nothing to do." % obj_name)
        return

      # Apply properties as defined in config file
      for att in self[base_name]:
        if att in exclude: continue
        # Get value and set respective attribute
        if not hasattr(o, "Set%s" % att): continue
        setattr(o, "Set%s" % att, self.value(base_name, att))


  def getAttValue(self, exclude=[], only=[]):
 
    commands = []

    for sec in self:
      if len(only) != 0:
        if sec not in only: continue
      elif len(exclude) != 0:
        if sec in exclude: continue
      for att in self[sec]:
        # Get the value
        commands.append((att, self.value(sec, att)))

    return commands


  def value (self, section, key, type="auto"):

    title = self[section][key]
    # Convert to acsii format
    title = unicodedata.normalize("NFKD", title).encode("ascii", "ignore")
    if title != self.delimiter:
      title = title.split(self.delimiter)
    else:
      title = [title]
    # Split based on delimiter
    if type != "auto":
      return [type(item) for item in title]
    else:
      # First, check if data is number
      if not all(_is_dgit(item) for item in title):
        return _get_return(title, str)
      else:
        # Check if integer
        if all(_represents_number(item, type=int) for item in title):
          return _get_return(title, int)
        elif all(_represents_number(item, type=float) for item in title):
          return _get_return(title, float)


if __name__ == "__main__":

  config = ConfigReader()
  h = ROOT.TH1F()
  config.apply2obj(h)
  config.apply()
