# Basic import(s)
import os
import gc
import ROOT


def garbage_collect (f):
  """
  Function decorator to manually perform garbage collection after the call,
  so as to avoid unecessarily large memory consumption.
  """

  def wrapper(*args, **kwargs):
    ret = f(*args, **kwargs)
    gc.collect()
    return ret

  return wrapper


def mkdir (path):

  """Script to ensure that the directory at `path` exists.

  Arguments:
      path: String specifying path to directory to be created.
  """

  # Check mether  output directory exists
  if not os.path.exists(path):
    try:
       os.makedirs(path)
    except OSError:
      # Apparently, 'path' already exists.
      pass
    pass
  return


def purge (dir_, pattern):

  import re
  for f in os.listdir(dir_):
    if re.search(pattern, f):
      os.remove(os.path.join(dir_, f))


def dump_argparse(args, fname):

  import json
  with open(fname, "w") as f:
    json.dump(args.__dict__, f, indent=2)


def load_argparse(args, fname):

  import json
  with open(fname, "w") as f:
    args.__dict__ = json.load(f)


def norm_h(hist, norm=1.0):

  if "TH1" in hist.ClassName(): integral = hist.Integral(0, hist.GetNbinsX() + 1)
  elif "TH2" in hist.ClassName(): integral = hist.Integral(0, hist.GetNbinsX() + 1, 0, hist.GetNbinsY() + 1)
  # Normalize
  if float(integral) > 10E-05:
    hist.Scale(norm/float(integral))
  else:
    print("[WARNING] Area under the histogram below 10E-05. The histogram will not be normalized to avoid `division by zero`")


class Quiet:
    """Context manager for silencing certain ROOT operations.  Usage:
    with Quiet(level = .kInfo+1):
       foo_that_makes_output

    You can set a higher or lower warning level to ignore different
    kinds of messages.  After the end of indentation, the level is set
    back to what it was previously.
    """
    def __init__(self, level=ROOT.kInfo + 1):
        self.level = level

    def __enter__(self):
        self.oldlevel = ROOT.gErrorIgnoreLevel
        ROOT.gErrorIgnoreLevel = self.level

    def __exit__(self, type, value, traceback):
        ROOT.gErrorIgnoreLevel = self.oldlevel
