# Machine Learning Related Stuff

This is a small, personalized python packages that includes a collection of stand-alone helper functions as well as classes that are supposed to make the daily life of a Ph.D student in high-energy physics easier.

# Authors

If something is not clear or you experience any problems or spot nasty bugs of any kind, do not hesitate to contact:

- Christof Sauer, <christof.sauer@cern.ch>, Ruprecht-Karls Universität Heidelberg, Germany (main developer)

# Installation

There are two ways to install this software: using pip or cloning the repository and installing the software manually.

## Using pip

If you want to install the package via pip, just open a terminal and execute the following command

```bash
pip install git+https://gitlab.com/csauer/myml.git
```

This is the recommended way; however, you won't be able to modify the source code though.

## Cloning the repository

Cloning the entire repository gives you more freedom since it provides you access to the source code and hence allows you apply changes according to your needs. The installation of the package is quite easy. All dependencies are summaried in the *requirements.txt* file located in the **install** directory. To install the package, all you have to do is to execute the Makefile script in the root directory by calling:

```bash
make
```

That's all. After those steps, you can import the package from anywhere by including *import myml* in yout python scripts.

## Simple example

```python
import ROOT
import random

# Create some histograms
ha = ROOT.TH1F("A", "Title for A,,l", 30, 0, 1)
hb = ROOT.TH1F("B", "B", 30, 0, 1)
hc = ROOT.TH1F("C", "C", 30, 0, 1)
# Also create a ROOT file and save histograms to it
tF = ROOT.TFile("Out.root", "RECREATE")
for i in range(100000):
  ha.Fill(random.random())
  hb.Fill(random.random())
  hc.Fill(random.random())
ha.Write()
hb.Write()
# Close file and load histogram(s) in plotting scripts
tF.Close()

from myplt.figure import Figure
# Start plotting here
Figure().fromFile("Out.root", ["A", "B"]) \
        .addHist1D(hc) \
        .addAxisTitle(title_x="Title [GeV]") \
        .addLegend(title="DNN top tagger") \
        .normalize() \
        .addTimeStamp() \
        .addRatio("A", "B") \
        .addAxisTitle(title_y="A/B") \
        .save("output2.pdf") \
        .dumpToROOT("output_ratio.root")

# Just awesome ...
```
