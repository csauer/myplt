from setuptools import setup, find_packages

import os, sys
path2dir = os.path.dirname(os.path.realpath(__file__))

install_requires = []
if os.path.isfile(os.path.join(path2dir, "install/requirements.txt")):
  with open(os.path.join(path2dir, "install/requirements.txt")) as f:
    install_requires = f.read().splitlines()

setup(
  name="myplt",
  version='0.0.1',
  description="My personal python package for plotting with CERN's ROOT",
  url="git@gitlab.com:csauer/myplt.git",
  author="Christof Sauer",
  author_email="csauer@cern.ch",
  license="unlicense",
  packages=find_packages(),
  include_package_data = True,
  dependency_links = [
    "git+https://gitlab.com/csauer/myutils.git"
    "git+https://gitlab.com/csauer/myroot.git"
    "git+https://gitlab.com/csauer/myhep.git"
    "git+https://gitlab.com/csauer/myml.git"],
  package_data = {
    # If any package contains *.ini files, include them
    '': ['*.ini'],
  },
  zip_safe=False
)

#install_requires=install_requires,
