#!/usr/bin/env python

import ROOT
import random
import myplt

# Create some histograms
ha = ROOT.TH1F("A", "Title for A,,l", 30, 0, 1)
hb = ROOT.TH1F("B", "B", 30, 0, 1)
hc = ROOT.TH1F("C", "C", 30, 0, 1)
# Also create a ROOT file and save histograms to it
tF = ROOT.TFile("Out.root", "RECREATE")
for i in range(100000):
  ha.Fill(random.random())
  hb.Fill(random.random())
  hc.Fill(random.random())
ha.Write()
hb.Write()
# Close file and load histogram(s) in plotting scripts
tF.Close()

from myplt.figure import Figure
# Start plotting here
Figure().fromFile("Out.root", ["A", "B"]) \
        .addHist1D(hc) \
        .addAxisTitle(title_x="Title [GeV]") \
        .addLegend(title="DNN top tagger") \
        .normalize() \
        .addTimeStamp() \
        .addRatio("A", "B") \
        .addAxisTitle(title_y="A/B") \
        .save("output2.pdf") \
        .dumpToROOT("output_ratio.root")

