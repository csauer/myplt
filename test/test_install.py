#!/usr/bin/env python

try:
  import myplt
  print("[TEST] Congratulations, myplt is now installed on your system :).")
except ImportError, e:
  print("[WARNING] Module `hep` is not installed on your system. Something went wrong.")
