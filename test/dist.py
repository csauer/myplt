#!/usr/bin/env python2

# System imports
import os, sys

# Scientific imports
import ROOT


def _run ():

  # Open files
  fROOT = ROOT.TFile("dat/outputPythiaNominal_nUnfold4_nSDB00Z00.root", "READ")

#  tc = ROOT.TCanvas("", "", 500, 500)
#  h1 = fROOT.Get("Unfold")
#  h1.SetFillColor(ROOT.kRed)
#  h1.Draw("E2")
#  tc.SaveAs("x.pdf")

  # Some global plotting settings
  import myplt.settings
  myplt.settings.file.extension = ["pdf", "png"]
  myplt.settings.common_head() \
    .addRow(text="#sqrt{s}=13 TeV, |#eta|<2.0, p^{truth}_{T} > 350 GeV") \
    .setTextAlign("left")
  myplt.settings.common_text() \
    .addExperiment()  \
    .addText("Anti-k_{t} R=0.4 track jets") \
  # Legend
  myplt.settings.legend.x1 = 0.6
  myplt.settings.legend.x2 = 0.9

  from myplt.figure import Figure


  h0 = fROOT.Get("Unfold")
  h1 = fROOT.Get("Unfold")
  h1.SetFillColorAlpha(ROOT.kRed, 0.3)
  h1.SetFillStyle(3244)

  
  # Unfolded histogram
  myplt.settings.histo.marker_style = 8
  Figure(canv=(600,500)) \
    .addHist(h1, draw_option="PMC PLC E2") \
    .addAxisTitle(
      title_x="Number of soft drop splittings n_{SD}") \
    .save("plt/unfold").close()

#  # Reco_systematic
#  Figure(canv=(600,500)) \
#    .setDrawOption("PMC PLC HIST") \
#    .addHist(fROOT.Get("Reco_systematic")) \
#    .addAxisTitle(
#      title_x="Number of soft drop splittings n_{SD}") \
#    .save("plt/reco_systematic").close()
#
#  # Reco_nominal
#  Figure(canv=(600,500)) \
#    .setDrawOption("PMC PLC HIST") \
#    .addHist(fROOT.Get("Reco_nominal")) \
#    .addAxisTitle(
#      title_x="Number of soft drop splittings n_{SD}") \
#    .save("plt/reco_nominal").close()
#
#  # Fakes_systematic
#  Figure(canv=(600,500)) \
#    .setDrawOption("PMC PLC HIST") \
#    .addHist(fROOT.Get("Fakes_systematic")) \
#    .addAxisTitle(
#      title_x="Number of soft drop splittings n_{SD}") \
#    .save("plt/fakes_systematic").close()
#
#  # Response
#  h2_resp = fROOT.Get("Response_nominal")
#  h2_resp.Scale( 1./h2_resp.Integral() )
#  Figure(canv=(600,500)) \
#    .setDrawOption("COLZ") \
#    .setColorPalette(51) \
#    .addHist(h2_resp) \
#    .addAxisTitle(
#      title_x="Detector level n_{SD}",
#      title_y="particle level n_{SD}",
#      title_z="P(particle level | detector level)") \
#    .setPadMargin() \
#    .setRangeZ(0, h2_resp.GetMaximum()) \
#    .save("plt/response_nominal").close()


if __name__ == "__main__":

  _run()
